import { ToasterService } from './../../toaster.service';
import { OtpDialogService } from './otp-dialog.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
export interface OTPModel {
  email: string;
  userName:string;
  otpEntered:number;
}
@Component({
  selector: 'app-otp-dialog',
  templateUrl: './otp-dialog.component.html',
  styleUrls: ['./otp-dialog.component.css'],
  providers: [OtpDialogService]
})
export class OtpDialogComponent {
  invalidOtp: boolean = false;
  constructor(
    public dialogRef: MatDialogRef<OtpDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: OTPModel,private otpService: OtpDialogService,
    private toaster: ToasterService) {
  }

  verifyOtp() {
    this.invalidOtp = false;
    this.otpService.verifyOtp(this.data).subscribe(result =>{
      this.invalidOtp = !result;
      if(result) {
        this.toaster.success('OTP Verified! Login to Continue.');
        this.dialogRef.close(true);
      }
    });
  }

  closeDialog() {
    this.dialogRef.close(false);
  }
  
  resendOtp() {
    this.otpService.resendOtp(this.data).subscribe(result =>{
      if(result) {
        this.toaster.info('OTP sent successfully to '+ this.data.email);
      } else {
        this.toaster.error('Error sending OTP');
      }
    });
  }
}
