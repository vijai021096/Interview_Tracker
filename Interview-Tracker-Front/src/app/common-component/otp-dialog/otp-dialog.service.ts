import { AppConfig } from './../../core/app-config';
import { Observable } from 'rxjs';
import { OTPModel } from './otp-dialog.component';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OtpDialogService {
  baseUrl = AppConfig.API_BASE_PATH + '/api/user/noAuth'
  constructor(private _http: HttpClient) { }

  verifyOtp(otpRequest: OTPModel):Observable<boolean> {
    return this._http.post<boolean>(this.baseUrl+ '/verifyOtp',otpRequest);
  }

  resendOtp(otpRequest: OTPModel):Observable<boolean> {
    return this._http.post<boolean>(this.baseUrl + '/resendOtp',otpRequest);
  }
}
