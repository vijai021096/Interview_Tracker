export declare type ColumnType = 'string' | 'icon' | 'date' | 'currency' | 'stringList';
export declare type IconColor = 'warn' | 'primary' | 'accent';
export interface SearchColumnDef {
    columnId:string;
    columnName:string;
    columnType?: ColumnType;
    iconType?:string;
    iconColor?:IconColor;
    dateFormat?:string;
    currencyPipe?:string;
}
export interface GridActionDef {
    actionId:string;
    actionName:string;
}