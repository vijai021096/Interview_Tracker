import { Observable } from 'rxjs';
export interface SearchApi<T> {
    /** return the fetched result array */
    search(keyword: string,offSet:number):Observable<T[]>;
}