import { MatChipInputEvent } from '@angular/material/chips';
import { Component, OnInit, ViewChild, ElementRef, EventEmitter, Output, Input } from '@angular/core';
import { MatAutocompleteSelectedEvent, MatAutocomplete } from '@angular/material/autocomplete';
import { startWith, map } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { COMMA, ENTER } from '@angular/cdk/keycodes';

@Component({
  selector: 'skill-list',
  templateUrl: './skill-list.component.html',
  styleUrls: ['./skill-list.component.css']
})
export class SkillListComponent implements OnInit{

  visible = true;
  selectable = true;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  skillCtrl = new FormControl();
  dummyControl = new FormControl();
  filteredSkills: Observable<string[]>;
  @Input() selectedSkills: string[] = [];
  @Input() skillLabel: string = '';
  @Input() containerEnabled: boolean = false;
  @Input() isDisabled: boolean = false;
  @Input() backgroundColor: string = "#FFF";
  @Output() onChange: EventEmitter<string[]> = new EventEmitter<string[]>();
  allSkills: string[] = ['Spring Boot', 'Spring Core', 'AngularJs', 'Spring MVC',
    'Kafka', 'Elastic DB', 'Struts', 'Microservices', 'Java 1.8', 'Java 1.7+',
    'Hibernate', 'Multithreading', 'HTML', 'CSS', 'Typescript', 'Angular 2+', 'Angular 7+',
    'Git', 'Javascript', 'OOPS', 'Design Patterns', 'Python', 'Django', 'Machine Learning', 'BigData',
    'Hadoop', 'PostgreSQL', 'MYSQL', 'PL/SQL', 'Oracle 12c', 'Oracle 11g', 'React',
    'React native', 'Ioninc', 'Native script', 'RabitMQ'];

  @ViewChild('skillInput') skillInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;
  constructor() {
    this.allSkills.sort();
    this.filteredSkills = this.skillCtrl.valueChanges.pipe(
      startWith(null),
      map((fruit: string | null) => fruit ? this._filter(fruit) : this.allSkills.slice()));
  }

  ngOnInit() {
    this.allSkills = this.allSkills.filter(skill => !this.selectedSkills.includes(skill));
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    if ((value || '').trim()) {
      this.selectedSkills.push(value.trim());
    }
    if (input) {
      input.value = '';
    }
    this.skillCtrl.setValue(null);
  }

  remove(skill: string): void {
    const index = this.selectedSkills.indexOf(skill);

    if (index >= 0) {
      const removedSkill = this.selectedSkills[index];
      this.allSkills.push(removedSkill);
      this.selectedSkills.splice(index, 1);
      this._resetAllSkills();
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    const skill = event.option.value;
    if (!this.selectedSkills.includes(skill)) {
      this.selectedSkills.push(skill);
      this.allSkills = this.allSkills.filter(filterSkill => filterSkill != skill);
      this._resetAllSkills();
    }
  }

  private _resetAllSkills(): void {
    this.allSkills.sort();
    this.skillInput.nativeElement.value = '';
    this.skillCtrl.setValue(null);
    this.skillInput.nativeElement.blur();
    this.onChange.emit(this.selectedSkills);
  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.allSkills.filter(fruit => fruit.toLowerCase().indexOf(filterValue) === 0);
  }

}
