import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { CanDeactivateApi } from './confirmation-guard.api';
@Injectable({
    providedIn: 'root'
})
export class ConfirmDeactivateGuard implements CanDeactivate<CanDeactivateApi> {
    canDeactivate(component: CanDeactivateApi, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): boolean | import("@angular/router").UrlTree | import("rxjs").Observable<boolean | import("@angular/router").UrlTree> | Promise<boolean | import("@angular/router").UrlTree> {
        return component.isSaved();
    }
    
}