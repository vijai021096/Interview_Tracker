export interface CanDeactivateApi {
    isSaved(): Promise<boolean>;
}