export const AppConfig =  {
    API_BASE_PATH: '/HireApi',
    ACCESS_LEVEL: {
        ADMIN: 'ADMIN',
        HR: 'HR',
        CANDIDATE:'CANDIDATE'
    },
    SOCKET_ENDPOINT:'http://localhost:3000'
}