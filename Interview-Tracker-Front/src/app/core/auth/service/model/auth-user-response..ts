export interface AuthUserResponse {
    userName: string;
    roles: Array<string>;
}