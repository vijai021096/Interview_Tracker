export interface JobStatusModel {
    closed:number;  
    open:number;
}

export interface JobsAndCandidatesPerStatus {
    jobName:string;
    numberOfCandidates:number;
    jobId:number;
}

export interface JobOpeningStatus {
    filled: number;
    remaining: number;
}