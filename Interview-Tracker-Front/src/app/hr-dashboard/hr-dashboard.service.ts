import { JobsAndCandidatesPerStatus, JobStatusModel, JobOpeningStatus } from './models/hr-dashboard';
import { Observable } from 'rxjs';
import { AppConfig } from './../core/app-config';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HrDashboardService {
  baseUrl: string = AppConfig.API_BASE_PATH+'/api'
  constructor(private _http: HttpClient) { }

  getJobsGroupedByStatus(userName:string): Observable<JobStatusModel> {
    return this._http.get<JobStatusModel>(this.baseUrl+'/job/hr/getJobsGroupedByStatus/'+userName);
  }

  getAllCandidatesPerJobStatus(status: string,userName: string): Observable<JobsAndCandidatesPerStatus[]> {
    return this._http.get<JobsAndCandidatesPerStatus[]>(this.baseUrl+'/candidate/hr/getAllCandidatesPerJobStatus?status='+status+'&userName='+userName);
  }

  getJobOpeningStatus(jobId: number):Observable<JobOpeningStatus> {
    return this._http.get<JobOpeningStatus>(this.baseUrl+'/job/hr/getJobOpeningStatus/'+jobId);
  }
}
