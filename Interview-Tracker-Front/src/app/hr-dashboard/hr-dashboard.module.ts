import { AppConfig } from './../core/app-config';
import { RolesAuthGuard } from './../core/auth/guard/roles-auth-guard';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HrDashboardComponent } from './hr-dashboard.component';
import { GoogleChartsModule } from 'angular-google-charts';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

// const routes: Routes = [{path:'',canActivate: [RolesAuthGuard],component:HrDashboardComponent, data:{accessLevel:AppConfig.ACCESS_LEVEL.HR}}]
@NgModule({
  declarations: [HrDashboardComponent],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    GoogleChartsModule
  ],
  exports: [GoogleChartsModule,HrDashboardComponent]
})
export class HrDashboardModule { }
