import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewCandidatesHrComponent } from './view-candidates-hr.component';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { SearchPageModule } from '../common-component/search-page/search-page.module';
import { RolesAuthGuard } from '../core/auth/guard/roles-auth-guard';
import { AppConfig } from '../core/app-config';




const routes:Routes=[{path:'',canActivate:[RolesAuthGuard],data:{accessLevel:AppConfig.ACCESS_LEVEL.HR},component: ViewCandidatesHrComponent}]
@NgModule({
  declarations: [ViewCandidatesHrComponent],
  imports: [
    CommonModule,
    SearchPageModule,
    RouterModule.forChild(routes)
  ]
})
export class ViewCandidatesHrModule { }
