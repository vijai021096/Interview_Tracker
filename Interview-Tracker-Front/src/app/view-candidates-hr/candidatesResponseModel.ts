export class CandidateResponseModel{

    candidateName:string
    emailId:string
    roundNumber:number
    status:string
    totalRounds:number;
    sheduledDate:Date;
    time: string;
    matchingPercentage:string
}