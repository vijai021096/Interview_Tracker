import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ReactiveFormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-interview-sheduler-dialog',
  templateUrl: './interview-sheduler-dialog.component.html',
  styleUrls: ['./interview-sheduler-dialog.component.css']
})
export class InterviewShedulerDialogComponent implements OnInit {
  date: Date;
  interviewshedulerform: FormGroup;

  constructor(private formBuilder: FormBuilder, public dialogRef: MatDialogRef<InterviewShedulerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { date, time }) { }

  ngOnInit(): void {
    this.interviewshedulerform = this.formBuilder.group({
      Date: ['', Validators.required],
      Time: [{}, [Validators.required, Validators.minLength(3)]]
    });
  }
  sendResponse(schedule: boolean) {
    if (schedule) {
      const hour = this.interviewshedulerform.value.Time.hour;
      let Meridian = 'AM';
      if (hour === 0) {
        this.interviewshedulerform.value.Time.hour = 12;
      } else if (hour > 12) {
        this.interviewshedulerform.value.Time.hour = hour - 12;
        Meridian = 'PM';
      } else if(hour === 12) {
        Meridian = 'PM';
      }
      const minute = this.interviewshedulerform.value.Time.minute;
      if(minute <10) {
        this.interviewshedulerform.value.Time.minute = '0' + minute;
      }
      const modifiedTime = this.interviewshedulerform.value.Time.hour;
      if(modifiedTime < 10) {
        this.interviewshedulerform.value.Time.hour = '0' + modifiedTime;
      }
      const time = this.interviewshedulerform.value.Time.hour + '.' + this.interviewshedulerform.value.Time.minute + ' ' + Meridian;
      this.dialogRef.close({ result: schedule, data: { date: this.date, time: time } });
    } else {
      this.dialogRef.close({ result: schedule, data: { date: this.date, time: "" } });
    }
  }

}
