import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { MatMenuModule } from '@angular/material/menu';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../core/auth/guard/auth-guard';
import { RolesAuthGuard } from '../core/auth/guard/roles-auth-guard';
import { AppConfig } from '../core/app-config';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MATERIAL_MODULE } from '../core/material-constant-module-array';
import { HrDashboardModule } from '../hr-dashboard/hr-dashboard.module';
import { CandidateChartModule } from '../candidate-chart/candidate-chart.module';
import { CommonComponentModule } from '../common-component/common-component.module';

const routes: Routes = [{path:'',component:HomeComponent, children: [  
  { path: 'job', canActivate: [AuthGuard], loadChildren: () => import('.././view-job/view-job.module').then(module => module.JobModule) },
  { path: 'assignRoles', canActivate: [AuthGuard], loadChildren: () => import('.././assign-roles/assign-roles.module').then(module => module.AssignRolesModule) },
  { path: 'addjob', canActivate: [AuthGuard], loadChildren: () => import('.././add-job/add-job.module').then(module => module.AddJobModule) },
  { path: 'viewhrjob', canActivate: [AuthGuard], loadChildren: () => import('.././view-job-hr/view-job-hr.module').then(module => module.ViewJobHrModule) },
  { path: 'viewcandidatehr', canActivate: [AuthGuard], loadChildren: () => import('.././view-candidates-hr/view-candidates-hr.module').then(module => module.ViewCandidatesHrModule) },
  { path: 'profile',canActivate: [AuthGuard],loadChildren: () => import('../profile/profile.module').then(module => module.ProfileModule)}
]}]

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    MatMenuModule,
    CommonComponentModule,
    HrDashboardModule,
    CandidateChartModule,
    MatSidenavModule,
    RouterModule.forChild(routes),
    MATERIAL_MODULE,
  ],
  exports:[HomeComponent]
})
export class HomeModule { }
