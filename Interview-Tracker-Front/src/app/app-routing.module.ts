import { AppConfig } from './core/app-config';
import { RolesAuthGuard } from './core/auth/guard/roles-auth-guard';
import { CandidateChartComponent } from './candidate-chart/candidate-chart.component';
import { AuthGuard } from './core/auth/guard/auth-guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: 'register', loadChildren: () => import('./user-registration/user-registration.module').then(module => module.UserRegistrationModule) },
  { path: 'login', loadChildren: () => import('./login/login.module').then(module => module.LoginModule) },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'home', loadChildren: () => import('./home/home.module').then(module => module.HomeModule), canActivate: [AuthGuard]
  }

]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {


}
