import { MatSnackBar } from '@angular/material/snack-bar';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class ToasterService {

  constructor(private snackBar: MatSnackBar) {
  }
  success(message: string, action?:string) {
    this.snackBar.open(message, action? action: 'X', {panelClass:'toast-success'});
  }

  warning(message: string, action?:string) {
    this.snackBar.open(message,action? action: 'X', {panelClass:'toast-warning'});
  }

  error(message: string, action?:string) {
    this.snackBar.open(message,action? action: 'X', {panelClass:'toast-error'});
  }
  info(message: string, action?:string) {
    this.snackBar.open(message,action? action: 'X', {panelClass:'toast-info'});
  }
}
