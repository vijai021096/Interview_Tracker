import { AbstractControl, ValidatorFn } from "@angular/forms";

export function checkUserNameValid(invalidNameExp: RegExp):ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
        return invalidNameExp.test(control.value)? {"invalidName": {value: control.value}}:null;
    }
}