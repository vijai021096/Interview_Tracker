import { ToasterService } from './../toaster.service';
import { Router } from '@angular/router';
import { OtpDialogComponent } from './../common-component/otp-dialog/otp-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { checkUserNameValid } from './validators/user-name-validator';
import { passwordValidator } from './validators/password-validator';
import { UserRegistrationService } from './Service/user-registration.service';

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.css']
})
export class UserRegistrationComponent implements OnInit {
  isValidName:boolean = true;
  isValidEmail:boolean = true;
  disableButton: boolean = false;
  registrationForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private service: UserRegistrationService,
    private dialog: MatDialog, private toaster: ToasterService, private router: Router) { }

  ngOnInit(): void {
    this.registrationForm = this.formBuilder.group({
      userName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(200), checkUserNameValid(/admin/)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    }, { validator: passwordValidator })
  }
  postDataToserver() {
    if (this.registrationForm.valid && this.isValidEmail && this.isValidName) {
      const params = this.registrationForm.value;
      this.disableButton=true;
      this.service.registerUser(params).subscribe(result => {
        this.disableButton=false;
        if (result['result']) {
          const dialogRef = this.dialog.open(OtpDialogComponent, {
            width: '300px',
            data: {
              email: this.registrationForm.value.email,
              userName: this.registrationForm.value.userName
            }
          });
          dialogRef.disableClose = true;
          dialogRef.afterClosed().subscribe(response => {
            this.router.navigateByUrl('/login');
          });
        }
      });
    }
  }

  checkUserNameAvailability() {
    if(this.registrationForm.get('userName').valid) {
      this.service.checkUserNameAvailability(this.registrationForm.value.userName).subscribe(result => {
        this.isValidName=result;
      });
    }
  }

  checkEmailAvailability() {
    if(this.registrationForm.get('email').valid) {
      this.service.checkEmailAvailability(this.registrationForm.value.email).subscribe(result => {
        this.isValidEmail=result;
      });
      console.log(this.registrationForm.get('email').errors);
    }
  }
}
