import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { UserRegistrationComponent } from './user-registration.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatInputModule } from '@angular/material/input'
import { UserRegistrationService } from './Service/user-registration.service';
const routes:Routes =[{path:'',component: UserRegistrationComponent}]

@NgModule({
  declarations: [UserRegistrationComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    MatInputModule
  ],
  providers:[
    UserRegistrationService
  ],
  exports:[UserRegistrationComponent]
})
export class UserRegistrationModule { }
