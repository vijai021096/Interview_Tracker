import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { AppConfig } from './../../core/app-config';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserRegistrationService {

  private baseUrl = AppConfig.API_BASE_PATH + "/api/user/noAuth";

  constructor(private httpClient:HttpClient) { }

  registerUser(params){
     return this.httpClient.post<any>(this.baseUrl+'/register',params);
  }

  checkUserNameAvailability(userName: string): Observable<boolean> {
    return this.httpClient.get<boolean>(this.baseUrl+'/checkUserNameAvailability/'+userName);
  }

  checkEmailAvailability(emailId: string): Observable<boolean> {
    return this.httpClient.get<boolean>(this.baseUrl+'/checkEmailAvailability/'+ emailId);
  }
}
