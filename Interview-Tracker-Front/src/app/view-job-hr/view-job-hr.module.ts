import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpClientModule } from '@angular/common/http';
import { ViewJobHrComponent } from './view-job-hr.component';
import { Router, RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { RolesAuthGuard } from '../core/auth/guard/roles-auth-guard';
import { AppConfig } from '../core/app-config';
import { SearchPageModule } from '../common-component/search-page/search-page.module';
import { SharedService } from '../common-component/shared.service';

const routes:Routes=[{path:'',canActivate:[RolesAuthGuard],component: ViewJobHrComponent,data:{accessLevel:AppConfig.ACCESS_LEVEL.HR}}]

@NgModule({
  declarations: [ViewJobHrComponent],
  imports: [
    CommonModule,
    SearchPageModule,
    RouterModule.forChild(routes)
  ]
 
})
export class ViewJobHrModule { }
