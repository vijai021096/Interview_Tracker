import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewJobHrComponent } from './view-job-hr.component';

describe('ViewJobHrComponent', () => {
  let component: ViewJobHrComponent;
  let fixture: ComponentFixture<ViewJobHrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewJobHrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewJobHrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
