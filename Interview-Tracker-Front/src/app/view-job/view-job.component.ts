import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatAccordion } from '@angular/material/expansion';
// import { MatStep } from '@angular/material/stepper';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatLabel } from '@angular/material/form-field';
import { HttpClient } from '@angular/common/http';
import { LocalStorageUtil } from '../core/auth/local-storage-util';
import { AppConfig } from '../core/app-config';
import { ViewJobService } from './view-job.service';
import { ToasterService } from '../toaster.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { STEP_STATE } from '@angular/cdk/stepper';
import {CurrencyPipe} from '@angular/common'

@Component({
  selector: 'view-job',
  templateUrl: './view-job.component.html',
  styleUrls: ['./view-job.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class JobComponent implements OnInit {
  isLinear = false;
  ShowDetails = true;
  panelOpenState = false;
  dataSource: any = [];
  dataAppliedSource: any = [];
  // columnsToDisplay = ['jobName', 'numberOfOpenings', 'Experience', 'jobShortDescription'];
  Object = Object;
  initColumns = [
    {
      "key": 'jobName',
      "name": "Job Name"
    },
    {
      "key": 'organisation',
      "name": "Organisation"
    },
    {
      "key": 'jobShortDescription',
      "name": "Job Short Description"
    },
    {
      "key": 'numberOfOpenings',
      "name": "No Of Openings"
    },
    {
      "key": 'minExperience',
      "name": "Minimum Experience"
    },
    {
      "key": 'maxExperience',
      "name": "Maximum Experience"
    },
    {
      "key": 'salaryRange',
      "name": "CTC"
    }
  ];
  initAppliedColumns = [
    {
      "key": 'jobName',
      "name": "Job Name"
    },
    {
      "key": 'organisation',
      "name": "Organisation"
    },
    {
      "key": 'jobShortDescription',
      "name": "Job Short Description"
    },
    {
      "key": 'status',
      "name": "Status"
    }
  ];
  columnsToDisplay: any[] = this.initColumns.map(col => col.key);
  columnsToDisplayApplied: any[] = this.initAppliedColumns.map(col => col.key);
  expandedElement: AllJobs | null;
  jobName: any;
  numberOfOpenings: any;
  jobShortDescription: any;
  Experience: any;
  minSalary:number;
  maxSalary:number;
  minsal:string;
  maxsal:string;
  salaryRange:string;
  JobDescription: any;
  responsibilities: any = [];
  skillsAndQualification: any = [];
  rounds: any = [];
  ResponsibilityList: any = [];
  skillsList: any = [];
  RoundsList: any = [];
  steps: any = [];
  stepps: any = ["hello", "jello"]
  candidateID: string;
  selectedStep: any = [];
  editable: any = [];
  stepControl: any;
  constructor(private _http: HttpClient, private cp: CurrencyPipe,private _formBuilder: FormBuilder, private _service: ViewJobService, private toaster: ToasterService) {}
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatAccordion, { static: true }) accordion: MatAccordion;
  @ViewChild(MatLabel, { static: true }) label: MatLabel;
  @ViewChild('stepper') stepper: MatStepper;
  
  
  ngOnInit() {
    this.viewJob();
  }

  ngAfterViewInit() {
    // this.stepper.selectedIndex = 1;
  }

  viewAppliedJobs() {
    this.steps = [];
    this._service.viewAppliedJob().subscribe(result => {
      this.dataAppliedSource = result;
      console.log("result", JSON.stringify(result));
      for (let i = 0; i < this.dataAppliedSource.length; i++) {
        this.selectedStep[i] = this.dataAppliedSource[i].roundNumber;
        this.dataAppliedSource[i].index = i;
        if (this.steps[i] == undefined) {
          this.steps[i] = [];
        }
        this.steps[i].push("Applied");
        for (let j = 0; j < this.dataAppliedSource[i].rounds.length; j++) {
          if (this.editable[i] == undefined) {
            this.editable[i] = [];
          }
          if (j == this.dataAppliedSource[i].roundNumber) {
            this.editable[i][j] = true;
          } else {
            this.editable[i][j] = false;
          }
          if (this.steps[i] == undefined) {
            this.steps[i] = [];
          }
          this.steps[i].push(this.dataAppliedSource[i].rounds[j].roundName)

        }
      }
      console.log("this.dataAppliedSource", this.dataAppliedSource)
      console.log("steps", this.steps)
      console.log("selectedSteps", this.selectedStep);
      console.log("editable", this.editable)
    });
  }

  viewJob() {
    this._service.viewJob().subscribe(result => {
      for(let i=0;i<result.unAppliedJobs.length;i++){
      if (result.unAppliedJobs[i].maxSalary != 0){
        this.minsal = this.cp.transform(result.unAppliedJobs[i].minSalary,"INR",true);
        this.maxsal = this.cp.transform(result.unAppliedJobs[i].maxSalary,"INR",true);
        result.unAppliedJobs[i].salaryRange = this.minsal+" - "+this.maxsal;
        // this.salaryRange = this.minsal+" - "+this.maxsal;
       }
       else{
         result.unAppliedJobs[i].salaryRange = 'Undisclosed';
       }
      }
      this.dataSource = new MatTableDataSource(result.unAppliedJobs);
      console.log("result", result);
      this.viewAppliedJobs();
    });
  }

  applyJob(event) {
    console.log(event);
    this._service.applyJob(LocalStorageUtil.getItem('userName'), event.jobId).subscribe(response => {
      if (response.result) {
        this.toaster.success("Applied Successfully!");
        this.viewJob();
      } else {
        this.toaster.error("Application not submitted.");
      }
    })
  }

}

export interface ApplyJobs {
  jobName: string;
  numberOfOpenings: number;
  organisation: string;
  jobShortDescription: string;
  minExperience: number;
  maxExperience: number;
  minSalary:number;
  maxSalary:number;
  responsibilities: Array<string>;
  skillsAndQualification: Array<string>;
  rounds: Array<object>;
}

export interface CandidateAppliedJobs {
  candidateName: string;
  jobName: string;
  organisation: string;
  jobShortDescription: string;
  status: string;
  competingWith : number;
  rounds: Array<object>;
  roundNumber: number;
  feedBack: string;
  scheduledDate: string;
}


export interface AllJobs {
  appliedJobs: Array<Object>;
  unAppliedJobs: Array<Object>;
}

export interface Jobs {
  jobName: string;
  numberOfOpenings: number;
  jobShortDescription: string;
  minExperience: number;
  maxExperience: number;
  responsibilities: Array<string>;
  skillsAndQualification: Array<string>;
  rounds: Array<object>;
}


const Available_Jobs: Jobs[] = [
  {
    jobName: 'Angular Developer',
    numberOfOpenings: 5,
    jobShortDescription: 'Develop and design innovative screens',
    minExperience: 2,
    maxExperience: 4,
    responsibilities: ["Hands on Experience in developing single page application using angular",
      "Strong Knowledge in angular 2, 4 and javascript",
      "UI skills",
      "Experience in tools like Jenkins, svn",
      "Proficiency in English and communication is must have.",
      "Proficiency with JavaScript and HTML5",
      "Professional, precise communication skills",
      "Deep knowledge of Angular practices and commonly used modules based on extensive work experience",
      "Creating self-contained, reusable, and testable modules and components",
      "Ensuring a clear dependency chain, in regard to the app logic as well as the file system",
      "Ability to provide SEO solutions for single page apps",
      "Extensive knowledge of CSS and JS methods for providing performant visual effects and keeping the framerate above 30fps at all times",
      "Thorough understanding of the responsibilities of the platform, database, API, caching layer, proxies, and other web services used in the system",
      "Validating user actions on the client side and providing responsive feedback",
      "Writing non-blocking code, and resorting to advanced techniques such as multi-threading, when needed",
      "Creating custom, general use modules, and components which extend the elements and modules of core Angular",
      "Experience with all levels of operation available to the front-end, such as from creating XHRs in vanilla JS to using a custom wrapper around $resource",
      "Experience with building the infrastructure for serving the front-end app and assets",
      "Architecting and automating the build process for production, using task runners or scripts",
      "Documenting the code inline using JSDoc or other conventions",
      "Writing extensive unit tests using automated TDD tasks",
      "Creating e2e test suites for all components, and running them with Protractor (or a well-reasoned alternative)",
      "Creating configuration, build, and test scripts for Continuous Integration environments"
    ],
    skillsAndQualification: ["Delivering a complete front-end application",
      "Ensuring high performance on mobile and desktop",
      "Writing tested, idiomatic, and documented JavaScript, HTML, and CSS",
      "Coordinating the workflow between the graphic designer, the HTML coder, and yourself",
      "Cooperating with the back-end developer in the process of building the RESTful API",
      "Communicating with external web services"
    ],
    rounds: [
      {
        "roundName": "Aptitude",
        "roundDescription": "Answer all aptitude questions quants,reasoning,logical and get 80% marks to clear the round"
      },
      {
        "roundName": "Coding Round",
        "roundDescription": "Answer all aptitude questions quants,reasoning,logical and get 80% marks to clear the round"
      },
      {
        "roundName": "Technical Round",
        "roundDescription": "Answer all aptitude questions quants,reasoning,logical and get 80% marks to clear the round"
      },
      {
        "roundName": "HR Round",
        "roundDescription": "Answer all aptitude questions quants,reasoning,logical and get 80% marks to clear the round"
      }
    ]
  },
  {
    jobName: 'React Developer',
    numberOfOpenings: 4,
    jobShortDescription: 'Develop and design innovative screens',
    minExperience: 2,
    maxExperience: 4,
    responsibilities: ["Hands on Experience in developing single page application using react",
      "Strong Knowledge in React and javascript",
      "UI skills",
      "Experience in tools like Jenkins, svn",
      "Proficiency in English and communication is must have.",
      "Proficiency with JavaScript and HTML5",
      "Professional, precise communication skills",
      "Deep knowledge of React practices and commonly used modules based on extensive work experience",
      "Creating self-contained, reusable, and testable modules and components",
      "Ensuring a clear dependency chain, in regard to the app logic as well as the file system",
      "Ability to provide SEO solutions for single page apps",
      "Extensive knowledge of CSS and JS methods for providing performant visual effects and keeping the framerate above 30fps at all times",
      "Thorough understanding of the responsibilities of the platform, database, API, caching layer, proxies, and other web services used in the system",
      "Validating user actions on the client side and providing responsive feedback",
      "Writing non-blocking code, and resorting to advanced techniques such as multi-threading, when needed",
      "Creating custom, general use modules, and components which extend the elements and modules of core React",
      "Experience with all levels of operation available to the front-end, such as from creating XHRs in vanilla JS to using a custom wrapper around $resource",
      "Experience with building the infrastructure for serving the front-end app and assets",
      "Architecting and automating the build process for production, using task runners or scripts",
      "Documenting the code inline using JSDoc or other conventions",
      "Writing extensive unit tests using automated TDD tasks",
      "Creating e2e test suites for all components, and running them with Protractor (or a well-reasoned alternative)",
      "Creating configuration, build, and test scripts for Continuous Integration environments"
    ],
    skillsAndQualification: ["Delivering a complete front-end application",
      "Ensuring high performance on mobile and desktop",
      "Writing tested, idiomatic, and documented JavaScript, HTML, and CSS",
      "Coordinating the workflow between the graphic designer, the HTML coder, and yourself",
      "Cooperating with the back-end developer in the process of building the RESTful API",
      "Communicating with external web services"
    ],
    rounds: [
      {
        "roundName": "Aptitude",
        "roundDescription": "Answer all aptitude questions quants,reasoning,logical and get 80% marks to clear the round"
      },
      {
        "roundName": "Coding Round",
        "roundDescription": "Answer all aptitude questions quants,reasoning,logical and get 80% marks to clear the round"
      },
      {
        "roundName": "Technical Round",
        "roundDescription": "Answer all aptitude questions quants,reasoning,logical and get 80% marks to clear the round"
      },
      {
        "roundName": "HR Round",
        "roundDescription": "Answer all aptitude questions quants,reasoning,logical and get 80% marks to clear the round"
      }
    ]
  }
];


const Applied_Jobs: Jobs[] = [
  {
    jobName: 'Java Developer',
    numberOfOpenings: 5,
    jobShortDescription: 'Develop and design innovative screens',
    minExperience: 2,
    maxExperience: 4,
    responsibilities: ["Hands on Experience in developing single page application using angular",
      "Strong Knowledge in angular 2, 4 and javascript",
      "UI skills",
      "Experience in tools like Jenkins, svn",
      "Proficiency in English and communication is must have.",
      "Proficiency with JavaScript and HTML5",
      "Professional, precise communication skills",
      "Deep knowledge of Angular practices and commonly used modules based on extensive work experience",
      "Creating self-contained, reusable, and testable modules and components",
      "Ensuring a clear dependency chain, in regard to the app logic as well as the file system",
      "Ability to provide SEO solutions for single page apps",
      "Extensive knowledge of CSS and JS methods for providing performant visual effects and keeping the framerate above 30fps at all times",
      "Thorough understanding of the responsibilities of the platform, database, API, caching layer, proxies, and other web services used in the system",
      "Validating user actions on the client side and providing responsive feedback",
      "Writing non-blocking code, and resorting to advanced techniques such as multi-threading, when needed",
      "Creating custom, general use modules, and components which extend the elements and modules of core Angular",
      "Experience with all levels of operation available to the front-end, such as from creating XHRs in vanilla JS to using a custom wrapper around $resource",
      "Experience with building the infrastructure for serving the front-end app and assets",
      "Architecting and automating the build process for production, using task runners or scripts",
      "Documenting the code inline using JSDoc or other conventions",
      "Writing extensive unit tests using automated TDD tasks",
      "Creating e2e test suites for all components, and running them with Protractor (or a well-reasoned alternative)",
      "Creating configuration, build, and test scripts for Continuous Integration environments"
    ],
    skillsAndQualification: ["Delivering a complete front-end application",
      "Ensuring high performance on mobile and desktop",
      "Writing tested, idiomatic, and documented JavaScript, HTML, and CSS",
      "Coordinating the workflow between the graphic designer, the HTML coder, and yourself",
      "Cooperating with the back-end developer in the process of building the RESTful API",
      "Communicating with external web services"
    ],
    rounds: [
      {
        "roundName": "Aptitude",
        "roundDescription": "Answer all aptitude questions quants,reasoning,logical and get 80% marks to clear the round"
      },
      {
        "roundName": "Coding Round",
        "roundDescription": "Answer all aptitude questions quants,reasoning,logical and get 80% marks to clear the round"
      },
      {
        "roundName": "Technical Round",
        "roundDescription": "Answer all aptitude questions quants,reasoning,logical and get 80% marks to clear the round"
      },
      {
        "roundName": "HR Round",
        "roundDescription": "Answer all aptitude questions quants,reasoning,logical and get 80% marks to clear the round"
      }
    ]
  },
  {
    jobName: 'Spring Developer',
    numberOfOpenings: 4,
    jobShortDescription: 'Develop and design innovative screens',
    minExperience: 2,
    maxExperience: 4,
    responsibilities: ["Hands on Experience in developing single page application using react",
      "Strong Knowledge in React and javascript",
      "UI skills",
      "Experience in tools like Jenkins, svn",
      "Proficiency in English and communication is must have.",
      "Proficiency with JavaScript and HTML5",
      "Professional, precise communication skills",
      "Deep knowledge of React practices and commonly used modules based on extensive work experience",
      "Creating self-contained, reusable, and testable modules and components",
      "Ensuring a clear dependency chain, in regard to the app logic as well as the file system",
      "Ability to provide SEO solutions for single page apps",
      "Extensive knowledge of CSS and JS methods for providing performant visual effects and keeping the framerate above 30fps at all times",
      "Thorough understanding of the responsibilities of the platform, database, API, caching layer, proxies, and other web services used in the system",
      "Validating user actions on the client side and providing responsive feedback",
      "Writing non-blocking code, and resorting to advanced techniques such as multi-threading, when needed",
      "Creating custom, general use modules, and components which extend the elements and modules of core React",
      "Experience with all levels of operation available to the front-end, such as from creating XHRs in vanilla JS to using a custom wrapper around $resource",
      "Experience with building the infrastructure for serving the front-end app and assets",
      "Architecting and automating the build process for production, using task runners or scripts",
      "Documenting the code inline using JSDoc or other conventions",
      "Writing extensive unit tests using automated TDD tasks",
      "Creating e2e test suites for all components, and running them with Protractor (or a well-reasoned alternative)",
      "Creating configuration, build, and test scripts for Continuous Integration environments"
    ],
    skillsAndQualification: ["Delivering a complete front-end application",
      "Ensuring high performance on mobile and desktop",
      "Writing tested, idiomatic, and documented JavaScript, HTML, and CSS",
      "Coordinating the workflow between the graphic designer, the HTML coder, and yourself",
      "Cooperating with the back-end developer in the process of building the RESTful API",
      "Communicating with external web services"
    ],
    rounds: [
      {
        "roundName": "Aptitude",
        "roundDescription": "Answer all aptitude questions quants,reasoning,logical and get 80% marks to clear the round"
      },
      {
        "roundName": "Coding Round",
        "roundDescription": "Answer all aptitude questions quants,reasoning,logical and get 80% marks to clear the round"
      },
      {
        "roundName": "Technical Round",
        "roundDescription": "Answer all aptitude questions quants,reasoning,logical and get 80% marks to clear the round"
      },
      {
        "roundName": "HR Round",
        "roundDescription": "Answer all aptitude questions quants,reasoning,logical and get 80% marks to clear the round"
      }
    ]
  }
];
