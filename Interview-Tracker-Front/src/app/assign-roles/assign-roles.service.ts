import { Observable } from 'rxjs';
import { AssignRolesRequestModel, AssignRolesResponseModel, UserRolesReponseModel } from './models/assign-roles.model';
import { AppConfig } from './../core/app-config';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiResult } from '../core/models/common-model';
@Injectable()
export class AssignRolesService {
    baseUrl: string = AppConfig.API_BASE_PATH + '/api/user/admin'
    constructor(private _http: HttpClient){}
    public getUsers(assingRolesrequest: AssignRolesRequestModel): Observable<AssignRolesResponseModel[]>  {
        return this._http.post<AssignRolesResponseModel[]>(this.baseUrl+'/getAllUsersExcept',assingRolesrequest);
    }

    public getUserRoles(userName: string):Observable<UserRolesReponseModel> {
        return this._http.get<UserRolesReponseModel>(this.baseUrl+'/getUserRoles/'+userName);
    }

    public updateRoles(updateRequest: AssignRolesResponseModel):Observable<ApiResult> {
        return this._http.post<ApiResult>(this.baseUrl+'/updateRoles',updateRequest);
    }
}