import { ToasterService } from './../toaster.service';
import { AssignRolesDialogComponent } from './assign-roels-dialog/assign-roles-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { ActionModel } from './../common-component/search-page/search-page.component';
import { ActivatedRoute } from '@angular/router';
import { LocalStorageUtil } from './../core/auth/local-storage-util';
import { AssignRolesResponseModel, AssignRolesRequestModel } from './models/assign-roles.model';
import { SearchApi } from './../common-component/search-page/api/search-api';
import { SearchColumnDef, GridActionDef } from './../common-component/search-page/models/search-column-def';
import { Component, OnInit } from '@angular/core';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { AssignRolesService } from './assign-roles.service';

@Component({
  selector: 'app-assign-roles',
  templateUrl: './assign-roles.component.html',
  styleUrls: ['./assign-roles.component.css']
})
export class AssignRolesComponent implements OnInit {
  displayColumnDefenition: SearchColumnDef[] = [
    {columnId:'userName',columnName:'User Name'},
    {columnId:'roles',columnName:'Role',columnType:'stringList'}
  ];
  api: SearchApi<AssignRolesResponseModel>;
  ready: boolean = false;
  placeholderText:string = "Search for User Name"
  rowActions:GridActionDef[] = [
    {
      actionId: 'manageRoles',
      actionName: 'Change Role'
    }
  ];
  refreshGrid: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  constructor(private _service: AssignRolesService,private route: ActivatedRoute,
     private dialog: MatDialog,private toaster: ToasterService) { }

  ngOnInit(): void {
      this.route.queryParams.subscribe(params =>{
        this.api = new AssignRolesSearchApi(this._service);
        this.ready = true;
      })
  }
  onRowClick(event) {
    console.log(event);
  }
  onAction(event: ActionModel<AssignRolesResponseModel>) {
    if(event.action ==='manageRoles') {
      this._service.getUserRoles(event.data.userName).subscribe(roleModel => {
        const dialogRef = this.dialog.open(AssignRolesDialogComponent,{
          width:'400px',
          data: roleModel
        });
        dialogRef.afterClosed().subscribe(dialogResponse =>{
          if(dialogResponse.result) {
            this._service.updateRoles({userName: event.data.userName,roles:[dialogResponse.roles.roleName]}).subscribe(resultResponse => {
              if(resultResponse.result) {
                this.toaster.success('Role Updated');
                this.refreshGrid.next(true); 
              } else {
                this.toaster.error('Role Update Failed');
              }
            });
          }
        });
      });
    }
    // this.refreshGrid.next(true);
  }

}
export class AssignRolesSearchApi implements SearchApi<AssignRolesResponseModel> {
  constructor(private _service: AssignRolesService) {}
  search(keyword: string, offSet: number): Observable<AssignRolesResponseModel[]> {
    const requestParam: AssignRolesRequestModel = {
      keyword: keyword,
      offset:offSet,
      userName: LocalStorageUtil.getItem('userName')
    };
    return this._service.getUsers(requestParam);
  }
  
}