export interface AssignRolesRequestModel {
    keyword:string;
    offset:number;
    userName:string;
}

export interface AssignRolesResponseModel {
    userName:string;
    roles:Array<string>;
}

export interface UserRolesReponseModel {
    roleName:string;
    alreadyGivenRole:boolean; 
}