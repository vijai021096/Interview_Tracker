import { AssignRolesDialogComponent } from './assign-roles/assign-roels-dialog/assign-roles-dialog.component';
import { AuthModule } from './core/auth/auth.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { ReactiveFormsModule } from '@angular/forms';

import { ConfirmationDialogComponent } from './common-component/confirmation-dialog/confirmation-dialog.component';
import { ToasterService } from './toaster.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'

import { APP_BASE_HREF, CurrencyPipe } from '@angular/common';
import { LoadingService } from './common-component/loading-service';
import { InformationDisplayDialogComponent } from './common-component/information-display-dialog/information-display-dialog.component';
import { UserRegistrationService } from './user-registration/Service/user-registration.service';
import { CommonHttpInteceptorService } from './common-http-inteceptor.service';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';
import { OtpDialogComponent } from './common-component/otp-dialog/otp-dialog.component';
import { InterviewShedulerDialogComponent } from './interview-sheduler-dialog/interview-sheduler-dialog.component';
import { FeedbackDialogComponent } from './feedback-dialog/feedback-dialog.component';
import { MATERIAL_MODULE } from './core/material-constant-module-array';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    InformationDisplayDialogComponent,
    ConfirmationDialogComponent,
    OtpDialogComponent,
    AssignRolesDialogComponent,
    InterviewShedulerDialogComponent,
    FeedbackDialogComponent    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    AuthModule.forRoot(),
    BrowserAnimationsModule,
    MATERIAL_MODULE,
    FontAwesomeModule,
    NgbModule
  ],
  entryComponents: [
    ConfirmationDialogComponent,
    InformationDisplayDialogComponent,
    OtpDialogComponent,
    AssignRolesDialogComponent,
    InterviewShedulerDialogComponent
  ],
  providers: [LoadingService, ToasterService, UserRegistrationService, CurrencyPipe, {
      provide: APP_BASE_HREF, useValue: '/Hire',
    },
    {
      provide: HTTP_INTERCEPTORS, useClass: CommonHttpInteceptorService, multi: true
    },
    {
      provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 4000 , horizontalPosition: 'right'}
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { } 
