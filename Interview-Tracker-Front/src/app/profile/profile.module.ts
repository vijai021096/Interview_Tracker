import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile.component';
import { Routes, RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { CommonComponentModule } from '../common-component/common-component.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ConfirmDeactivateGuard } from '../core/guards/confirmation.deactivate.guard';
import { MatButtonModule } from '@angular/material/button';

const routes: Routes = [{path:'',canDeactivate:[ConfirmDeactivateGuard],component:ProfileComponent}]
@NgModule({
  declarations: [ProfileComponent],
  imports: [
    CommonModule,
    CommonComponentModule,
    MatIconModule,
    MatFormFieldModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    FormsModule,
    MatRadioModule,
    MatButtonModule,
    MatSelectModule,
    MatChipsModule,
    MatInputModule,
    RouterModule.forChild(routes)
  ],
  exports:[ProfileComponent]
})
export class ProfileModule { }