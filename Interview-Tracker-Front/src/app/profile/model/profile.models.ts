export interface IndividualExperience {
    companyName: string;
    companyExp: number;
}

export interface profileDetails {
    userName: string;
    roles: string;
    gender: string;
    email: string;
    highestQualification: string;
    totalExp: number;
    skills: Array<string>;
    companyExpList: Array<IndividualExperience>;
}