import { OtpDialogComponent } from './../common-component/otp-dialog/otp-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { catchError } from 'rxjs/operators';
import { ToasterService } from './../toaster.service';
import { Router } from '@angular/router';
import { AuthService } from './../core/auth/service/auth-service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  userName: string;
  title:string = '</hire>'
  email: string;
  passWord: string;
registrationForm: FormGroup;
constructor(private formBuilder: FormBuilder,private authService:AuthService,
  private router: Router,private toaster:ToasterService, private dialog: MatDialog) { }

ngOnInit(): void {
  this.registrationForm = this.formBuilder.group({
    userName: ['', Validators.required],
    password: ['', Validators.required],
  });
  if(this.authService.isLoggedIn()) {
    this.router.navigateByUrl(this.authService.return);
  }
}
skillChange(skills: string[]) {
  console.log(skills);
}
postDataToserver() {
  if(this.registrationForm.valid) {
   const params=this.registrationForm.value;
   this.authService.perfomLogin(params).pipe(catchError(errorResponse => {
     if(errorResponse.status === 401) this.toaster.error('Bad Credentials');
     return errorResponse;
   })).subscribe(authResponse =>{
     if(!authResponse['authResult']) {
      this.toaster.error('Bad Credentials');
     }else if(authResponse['authResult'] && authResponse['verificationResult']) {
      this.router.navigateByUrl(this.authService.return)
    } else if(!authResponse['verificationResult']) {
      const dialogRef = this.dialog.open(OtpDialogComponent, {
        width: '300px',
        data: {
          email: this.registrationForm.value.email,
          userName: this.registrationForm.value.userName
        }
      });
      dialogRef.disableClose = true;
      dialogRef.afterClosed().subscribe(response => {
        if (response) {
          this.toaster.success('Logged In Successfully');
          this.router.navigateByUrl('/home');
        }
      });
    } else {
       this.toaster.error('Bad Credentials');
     }
   });
  } else {
    console.log('do Nothing');
  }
}

}
