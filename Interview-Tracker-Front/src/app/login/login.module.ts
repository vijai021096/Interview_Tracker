import { CommonComponentModule } from './../common-component/common-component.module';
import { OtpDialogComponent } from './../common-component/otp-dialog/otp-dialog.component';
import { LoginComponent } from './login.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthModule } from './../core/auth/auth.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input'

const routes: Routes = [{path:'',component:LoginComponent}]
@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    AuthModule,
    MatInputModule,
    CommonComponentModule,
    ReactiveFormsModule
  ],
  exports: [LoginComponent],
  entryComponents:[OtpDialogComponent]
})
export class LoginModule { }
