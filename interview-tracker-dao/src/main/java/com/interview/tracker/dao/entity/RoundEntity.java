package com.interview.tracker.dao.entity;

import java.util.Objects;

import com.interview.tracker.dao.dto.RoundDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RoundEntity {

	private int roundNo;
	private long jobId;
	private String roundName;
	private String roundDescription;
	
	public static RoundEntity formEntity(RoundDto dto) {
		if(Objects.isNull(dto)) return null;
		return RoundEntity.builder()
		.roundNo(dto.getRoundNo())
		.jobId(dto.getJob().getJobId())
		.roundName(dto.getRoundName())
		.roundDescription(dto.getRoundDescription())
		.build();
	}
	
	public static RoundDto formDto(RoundEntity entity) {
		if(Objects.isNull(entity)) return null;
		return RoundDto.builder()
		.roundName(entity.getRoundName())
		.roundNo(entity.getRoundNo())
		.roundDescription(entity.getRoundDescription())
		.build();
	}
}
