package com.interview.tracker.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.interview.tracker.dao.dto.SkillsAndQualificationsDto;

public interface JobSkillsRepository extends JpaRepository<SkillsAndQualificationsDto, Long> {

	@Query(value="SELECT SKILL FROM job_skills where job_id = :jobId",nativeQuery = true)
	public List<String> getSkillsByJobId(@Param("jobId") long jobId);
}
