package com.interview.tracker.dao.dao;

import org.springframework.beans.factory.annotation.Autowired;

import com.interview.tracker.dao.repository.RoleRepository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RoleDaoImpl implements RoleDao{

	private final RoleRepository roleRepository;
	@Override
	public boolean deleteRolesByUserId(long userId) {
		try {
			roleRepository.deleteRolesByUserId(userId);
			return true;
		} catch(Exception ex) {
			return false;
		}
	}

}
