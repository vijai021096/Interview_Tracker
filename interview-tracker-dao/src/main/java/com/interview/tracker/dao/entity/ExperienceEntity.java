package com.interview.tracker.dao.entity;

import java.util.Objects;

import com.interview.tracker.dao.dto.ExperienceDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ExperienceEntity {

	private long experienceId;

	private long userProfileId;

	private String companyName;

	private int experience;

	private UserEntity userProfile;

	public static ExperienceEntity formEntity(ExperienceDto dto) {
		if (Objects.isNull(dto))
			return null;

		return ExperienceEntity.builder().companyName(dto.getCompanyName()).experience(dto.getExperience())
				.experienceId(dto.getExperienceId()).userProfileId(dto.getUserProfile().getUserId()).build();
	}

	public static ExperienceDto formDto(ExperienceEntity entity) {
		if (Objects.isNull(entity))
			return null;

		return ExperienceDto.builder().companyName(entity.getCompanyName()).experience(entity.getExperience())
				.experienceId(entity.getExperienceId()).build();
	}
}
