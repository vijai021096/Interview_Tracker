package com.interview.tracker.dao.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CandidateJobKey implements Serializable{

	private static final long serialVersionUID = 1L;

	
	private Long jobId;
	
	private Long candidateId;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CandidateJobKey other = (CandidateJobKey) obj;
		if (candidateId != other.candidateId)
			return false;
		if (jobId != other.jobId)
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (candidateId ^ (candidateId >>> 32));
		result = prime * result + (int) (jobId ^ (jobId >>> 32));
		return result;
	}
	
	
	
}
