package com.interview.tracker.dao.dao;

import java.util.List;

import com.interview.tracker.dao.entity.ExperienceEntity;

public interface ExperienceDao {

	boolean deleteAllExperienceOfUser(long userId);

	List<ExperienceEntity> getUserExperience(long userId);
}
