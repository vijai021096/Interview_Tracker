package com.interview.tracker.dao.dao;

import java.util.List;

import com.interview.tracker.dao.entity.JobEntity;

/**
 * JobDao handles all job related data access
 * 
 * @author Somasundaram Muthusamy
 */
public interface JobDao {

	/**
	 * used to find a job by using the jobId
	 * 
	 * @param jobId, the job id for which the details is needed
	 * @return the job details
	 */
	public JobEntity findJobById(long jobId);

	/**
	 * used to add a new job
	 * 
	 * @param newJob, the details of the new job to be added
	 * @return true if job added successfully, false if job addition failed
	 */
	public boolean addNewJob(JobEntity newJob);

	/**
	 * used to fetch all the jobs that are applied by the candidate
	 * 
	 * @param candidateId, the id of the candidate for whom the result is needed
	 */
	public List<JobEntity> findAllJobsByCandidateName(String userName);

	/**
	 * used to fetch all the jobs that the candidate can apply at present
	 * 
	 * @param candidateId, the id of the candidate for whom the result is needed
	 */
	public List<JobEntity> findAllUnAppliedJobs(String userName);

	/**
	 * Used to fetch all jobs by person in charge.
	 * 
	 * @param personInCharge
	 * @return
	 */
	public List<JobEntity> findJobsByPersonInCharge(long personInCharge);

	public List<JobEntity> findJobsLikeJobName(String jobName, int offset,long pic);

	public List<JobEntity> findJobsFromJobIdArray(List<Long> candidateJobList);

	public List<JobEntity> getAllJobsPerStatus(String status, long userId);

	/**
	 * used to get all the responsible jobs irrespective of the status
	 * 
	 * @param inchargeId, the userId of the user
	 */
	public List<JobEntity> getAllInchargeJobs(long inchargeId);
}
