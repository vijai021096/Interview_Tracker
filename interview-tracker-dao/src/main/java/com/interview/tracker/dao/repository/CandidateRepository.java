package com.interview.tracker.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.interview.tracker.dao.dto.CandidateDto;

public interface CandidateRepository extends JpaRepository<CandidateDto, Long> {

	/**
	 * findCandidatesforAJob is used to fetch all the candidates who have applied
	 * for the job
	 * 
	 * @param jobId, the id of the job to search the candidates
	 */
	@Query(value = "SELECT * FROM candidates as candidate INNER JOIN jobs as job ON(candidate.job_id=job.job_id) "
			+ " WHERE job.job_id=:jobId and candidate.status NOT IN('SELECTED','REJECTED')", nativeQuery = true)
	public List<CandidateDto> findCandidatesforAJob(@Param("jobId") long jobId);

	@Query(value = "SELECT * FROM candidates where candidate_id=:candidateId and job_id=:jobId", nativeQuery = true)
	public List<CandidateDto> findAllCandidatesByCandidateIdAndJobId(@Param("candidateId") long candidateId,
			@Param("jobId") long jobId);

	@Query(value = "SELECT * FROM candidates where candidate_name=:candidateName", nativeQuery = true)
	public List<CandidateDto> findCandidates(@Param("candidateName") String candidateName);

	@Query("FROM CandidateDto as candidate WHERE candidate.jobId IN(:jobIds)")
	public List<CandidateDto> findAllCandidatesByMultipleJobIds(@Param("jobIds") List<Long> jobIds);

	@Query("FROM CandidateDto as candidate WHERE candidate.status='SELECTED' AND candidate.jobId=:jobId")
	public List<CandidateDto> getSelectedCandidatesByJobId(@Param("jobId") long jobId);

	@Query("FROM CandidateDto as candidate WHERE candidate.jobId IN (:candidateJobList)")
	public List<CandidateDto> getCandidatesByMultipleJobIds(@Param("candidateJobList") List<Long> candidateJobList);

	@Query(value = "SELECT * FROM candidates WHERE job_id=:jobId and status NOT IN('SELECTED','REJECTED') and candidate_name like %:candidateName% limit 20 offset :offset", nativeQuery = true)
	public List<CandidateDto> findCandidatesLikeNameforAJob(@Param("jobId") long jobId, @Param("candidateName")String candidateName, @Param("offset") int offset);
}
