package com.interview.tracker.dao.dao;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import com.interview.tracker.dao.entity.JobEntity;
import com.interview.tracker.dao.repository.JobRepository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class JobDaoImpl implements JobDao {

	private final JobRepository jobRepository;

	@Override
	public JobEntity findJobById(long jobId) {
		return JobEntity.formEntity(jobRepository.findById(jobId).orElse(null));
	}

	@Override
	public boolean addNewJob(JobEntity newJob) {
		try {
			jobRepository.save(JobEntity.formDto(newJob));
			return true;
		} catch (DataIntegrityViolationException exception) {
			return false;
		} catch (Exception exception) {
			return false;
		}
	}

	@Override
	public List<JobEntity> findAllJobsByCandidateName(String userName) {
		return jobRepository.findAllJobsByCandidateName(userName).stream().map(JobEntity::formEntity)
				.collect(Collectors.toList());
	}

	@Override
	public List<JobEntity> findAllUnAppliedJobs(String userName) {
		return jobRepository.findAllUnAppliedJobs(userName).stream().map(JobEntity::formEntity)
				.collect(Collectors.toList());
	}

	@Override
	public List<JobEntity> findJobsByPersonInCharge(long personIncharge) {

		return jobRepository.findAllJobsByPersonIncharge(personIncharge).stream().map(JobEntity::formEntity)
				.collect(Collectors.toList());

	}

	@Override
	public List<JobEntity> findJobsLikeJobName(String jobName, int offset,long pic) {

		return jobRepository.findAllJobsByJobName(jobName, offset,pic).stream().map(JobEntity::formEntity)
				.collect(Collectors.toList());
	}

	@Override
	public List<JobEntity> findJobsFromJobIdArray(List<Long> jobId) {
		return jobRepository.findJobsFromJobIdArray(jobId).stream().map(JobEntity::formEntity)
				.collect(Collectors.toList());
	}

	@Override
	public List<JobEntity> getAllJobsPerStatus(String status, long userId) {
		return jobRepository.findAllJobsPerStatus(status, userId).stream().map(JobEntity::formEntity)
				.collect(Collectors.toList());
	}

	@Override
	public List<JobEntity> getAllInchargeJobs(long inchargeId) {
		return jobRepository.getAllInchargeJobs(inchargeId).stream().map(JobEntity::formEntity)
				.collect(Collectors.toList());
	}

}
