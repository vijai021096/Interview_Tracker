package com.interview.tracker.dao.dao;

import java.util.List;

import org.springframework.data.repository.query.Param;

import com.interview.tracker.dao.dto.CandidateDto;
import com.interview.tracker.dao.entity.CandidateEntity;

public interface CandidateDao {
	/**
	 * used to fetch all the candidates who have applied for the job
	 * 
	 * @param jobId, the id of the job for which the result is needed
	 */
	public List<CandidateEntity> findCandidatesByJobId(long jobId);

	/**
	 * 
	 * @param candidateId
	 * @param jobId
	 * @return
	 */
	public List<CandidateEntity> findCandidateByCandidateIdAndJobId(long candidateId, long jobId);

	public boolean addCandidate(CandidateEntity candidate);

	public List<CandidateEntity> findCandidatesByCandidateName(String candidateName);

	public List<CandidateEntity> findAllCandidatesByMultipleJobIds(List<Long> jobIds);

	public List<CandidateEntity> getSelectedCandidatesByJobId(long jobId);

	public List<CandidateEntity> getCandidatesByMultipleJobIds(List<Long> candidateJobList);

	public boolean insertMultipleCandidates(List<CandidateEntity> insertCandidateList);

	public boolean addMultipleCandidates(List<CandidateEntity> rejectedCandidates);

	public List<CandidateEntity> findCandidatesLikeNameforAJob(long jobId, String candidateName, int offset);
	
}
