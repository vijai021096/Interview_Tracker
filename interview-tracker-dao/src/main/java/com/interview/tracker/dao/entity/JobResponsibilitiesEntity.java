package com.interview.tracker.dao.entity;

import java.util.Objects;

import com.interview.tracker.dao.dto.JobResponsibilitiesDto;
import com.interview.tracker.dao.dto.RoundDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class JobResponsibilitiesEntity {

	private long id;
	
	private String responsiblities;
	
	private long jobId;
	
	public static JobResponsibilitiesEntity formEntity(JobResponsibilitiesDto dto) {
		if(Objects.isNull(dto)) return null;
		return JobResponsibilitiesEntity.builder()
		  .responsiblities(dto.getResponsibilties())
		.build();
	}
	
	public static JobResponsibilitiesDto formDto(JobResponsibilitiesEntity entity) {
		if(Objects.isNull(entity)) return null;
		return JobResponsibilitiesDto.builder()
		   .Responsibilties(entity.getResponsiblities())
		.build();
	}
}
