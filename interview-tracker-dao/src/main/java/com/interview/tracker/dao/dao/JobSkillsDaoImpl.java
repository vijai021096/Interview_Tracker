package com.interview.tracker.dao.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.interview.tracker.dao.repository.JobSkillsRepository;


import lombok.RequiredArgsConstructor;
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class JobSkillsDaoImpl implements JobSkillsDao {

	private final JobSkillsRepository jobsSkillsRepository;
	@Override
	public List<String> getSkillsByJobId(long jobId) {

		return jobsSkillsRepository.getSkillsByJobId(jobId);
	}

}
