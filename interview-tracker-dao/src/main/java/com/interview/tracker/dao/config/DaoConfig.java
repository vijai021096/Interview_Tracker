package com.interview.tracker.dao.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.interview.tracker.dao.dao.CandidateDao;
import com.interview.tracker.dao.dao.CandidateDaoImpl;
import com.interview.tracker.dao.dao.ExperienceDao;
import com.interview.tracker.dao.dao.ExperienceDaoImpl;
import com.interview.tracker.dao.dao.JobDao;
import com.interview.tracker.dao.dao.JobDaoImpl;
import com.interview.tracker.dao.dao.JobSkillsDao;
import com.interview.tracker.dao.dao.JobSkillsDaoImpl;
import com.interview.tracker.dao.dao.RoleDao;
import com.interview.tracker.dao.dao.RoleDaoImpl;
import com.interview.tracker.dao.dao.UserDao;
import com.interview.tracker.dao.dao.UserDaoImpl;
import com.interview.tracker.dao.repository.CandidateRepository;
import com.interview.tracker.dao.repository.ExperienceRepository;
import com.interview.tracker.dao.repository.JobRepository;
import com.interview.tracker.dao.repository.JobSkillsRepository;
import com.interview.tracker.dao.repository.RoleRepository;
import com.interview.tracker.dao.repository.UserRepository;

@Configuration
public class DaoConfig {

	@Bean
	public JobDao jobDao(JobRepository jobRepository) {
		return new JobDaoImpl(jobRepository);
	}

	@Bean
	public UserDao userDao(UserRepository userRepository) {
		return new UserDaoImpl(userRepository);
	}

	@Bean
	public CandidateDao candidateDao(CandidateRepository candidateRepository) {
		return new CandidateDaoImpl(candidateRepository);
	}

	@Bean
	public RoleDao roleDao(RoleRepository roleRepository) {
		return new RoleDaoImpl(roleRepository);
	}

	@Bean
	public ExperienceDao experienceDao(ExperienceRepository expRepo) {
		return new ExperienceDaoImpl(expRepo);
	}

	@Bean
	public JobSkillsDao jobSkillsDao(JobSkillsRepository jobsSkillsRepository) {
		return new JobSkillsDaoImpl(jobsSkillsRepository);
	}
}
