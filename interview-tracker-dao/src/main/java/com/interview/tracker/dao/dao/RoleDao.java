package com.interview.tracker.dao.dao;

public interface RoleDao {

	public boolean deleteRolesByUserId(long userId);
	
}
