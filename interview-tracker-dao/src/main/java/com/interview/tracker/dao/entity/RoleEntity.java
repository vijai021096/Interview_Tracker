package com.interview.tracker.dao.entity;

import java.util.Objects;

import com.interview.tracker.dao.dto.RoleDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RoleEntity {
	
	private long roleId;

	private String roleName;
	
	private UserEntity userEntity;
	
	public static RoleEntity formEntity(RoleDto role) {
		if(Objects.isNull(role)) return null;
		return RoleEntity.builder()
				.roleId(role.getRoleId())
				.roleName(role.getRoleName())
				.build();
	}
	
	public static RoleDto formDto(RoleEntity entity) {
		if(Objects.isNull(entity)) return null;
		return RoleDto.builder()
			.roleId(entity.getRoleId())
			.roleName(entity.getRoleName())
			.build();
	}
}
