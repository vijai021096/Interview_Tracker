package com.interview.tracker.dao.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="round_table",uniqueConstraints = @UniqueConstraint(columnNames = {"job_id","round_no"}))
public class RoundDto {
	
	@Id
	@Column(name="round_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long roundId;
	
	@ManyToOne
	@JoinColumn(name="job_id",referencedColumnName = "job_id")
	private JobDto job;
	
	@Column(name = "round_no")
	private int roundNo;
	
	@Column(name="round_name")
	private String roundName;
	
	@Column(name="round_description")
	private String roundDescription;
	
}
