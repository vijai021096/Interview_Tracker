package com.interview.tracker.dao.entity;

import java.util.Date;
import java.util.Objects;

import com.interview.tracker.dao.dto.CandidateDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CandidateEntity {

	private long jobId;

	private String candidateName;

	private long candidateId;

	// TODO replace with enum
	private String status;

	private int roundNumber;

	private String emailId;

	private Date sheduledDate;

	private String time;

	private String feedBack;

	public static CandidateEntity formEntity(CandidateDto dto) {
		if (Objects.isNull(dto))
			return null;
		return CandidateEntity.builder().jobId(dto.getJobId()).candidateName(dto.getCandidateName())
				.candidateId(dto.getCandidateId()).status(dto.getStatus()).roundNumber(dto.getRoundNumber())
				.emailId(dto.getEmailId()).sheduledDate(dto.getSheduledDate()).time(dto.getTime())
				.feedBack(dto.getFeedBack()).build();
	}

	public static CandidateDto formDto(CandidateEntity entity) {
		if (Objects.isNull(entity))
			return null;
		CandidateDto job = CandidateDto.builder().jobId(entity.getJobId()).candidateName(entity.getCandidateName())
				.candidateId(entity.getCandidateId()).status(entity.getStatus()).roundNumber(entity.getRoundNumber())
				.emailId(entity.getEmailId()).feedBack(entity.getFeedBack()).sheduledDate(entity.getSheduledDate())
				.time(entity.getTime()).build();

		return job;
	}
}
