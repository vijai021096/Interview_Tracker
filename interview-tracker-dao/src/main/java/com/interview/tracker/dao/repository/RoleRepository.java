package com.interview.tracker.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.interview.tracker.dao.dto.RoleDto;

public interface RoleRepository extends JpaRepository<RoleDto, Long>{

	@Query(value="DELETE FROM user_role WHERE user_role.user_id=:userId",nativeQuery=true)
	@Modifying
	void deleteRolesByUserId(@Param("userId")long userId);

}
