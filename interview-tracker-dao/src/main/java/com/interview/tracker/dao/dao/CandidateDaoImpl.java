package com.interview.tracker.dao.dao;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import com.interview.tracker.dao.dto.CandidateDto;
import com.interview.tracker.dao.entity.CandidateEntity;
import com.interview.tracker.dao.repository.CandidateRepository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CandidateDaoImpl implements CandidateDao {
	private final CandidateRepository candidateRepository;

	@Override
	public List<CandidateEntity> findCandidatesByJobId(long jobId) {

		return candidateRepository.findCandidatesforAJob(jobId).stream().map(CandidateEntity::formEntity)
				.collect(Collectors.toList());

	}

	@Override
	public List<CandidateEntity> findCandidateByCandidateIdAndJobId(long candidateId, long jobId) {
		return candidateRepository.findAllCandidatesByCandidateIdAndJobId(candidateId, jobId).stream()
				.map(CandidateEntity::formEntity).collect(Collectors.toList());
	}

	@Override
	public boolean addCandidate(CandidateEntity candidate) {
		try {

			candidateRepository.save(CandidateEntity.formDto(candidate));
			return true;
		} catch (DataIntegrityViolationException exception) {
			return false;
		} catch (Exception exception) {
			return false;
		}
	}

	@Override
	public List<CandidateEntity> findCandidatesByCandidateName(String candidateName) {
		return candidateRepository.findCandidates(candidateName).stream().map(CandidateEntity::formEntity)
				.collect(Collectors.toList());
	}

	@Override
	public List<CandidateEntity> findAllCandidatesByMultipleJobIds(List<Long> jobIds) {
		return candidateRepository.findAllCandidatesByMultipleJobIds(jobIds).stream().map(CandidateEntity::formEntity)
				.collect(Collectors.toList());
	}

	@Override
	public List<CandidateEntity> getSelectedCandidatesByJobId(long jobId) {
		return candidateRepository.getSelectedCandidatesByJobId(jobId).stream().map(CandidateEntity::formEntity)
				.collect(Collectors.toList());
	}

	@Override
	public List<CandidateEntity> getCandidatesByMultipleJobIds(List<Long> candidateJobList) {
		return candidateRepository.getCandidatesByMultipleJobIds(candidateJobList).stream()
				.map(CandidateEntity::formEntity).collect(Collectors.toList());
	}

	@Override
	public boolean insertMultipleCandidates(List<CandidateEntity> insertCandidateList) {
		try {
			candidateRepository
					.saveAll(insertCandidateList.stream().map(CandidateEntity::formDto).collect(Collectors.toList()));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean addMultipleCandidates(List<CandidateEntity> rejectedCandidates) {
		try {
			candidateRepository
					.saveAll(rejectedCandidates.stream().map(CandidateEntity::formDto).collect(Collectors.toList()));
			return true;
		} catch (Exception ex) {
			return false;
		}
	}
	public List<CandidateEntity> findCandidatesLikeNameforAJob(long jobId, String candidateName, int offset) {
		
		return candidateRepository.findCandidatesLikeNameforAJob(jobId, candidateName, offset).stream().map(CandidateEntity::formEntity).collect(Collectors.toList());
	}

}
