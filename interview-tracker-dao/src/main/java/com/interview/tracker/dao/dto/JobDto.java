package com.interview.tracker.dao.dto;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "jobs")
public class JobDto {

	@Column(name = "job_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private long jobId;

	@Column(name = "job_name")
	private String jobName;

	@Column(name = "job_description")
	private String jobDescription;

	@Column(name = "organisation")
	private String organisation;
	
	@Column(name = "pic")
	private long personIncharge;

	@Column(name = "status")
	// TODO replace with enum
	private String status;

	@Column(name = "no_of_openings")
	private int numberOfOpenings;

	@Column(name = "min_experience")
	private int minExperience;

	@Column(name = "max_experience")
	private int maxExperience;

	@Column(name = "num_of_rounds")
	private int numOfRounds;
	
	@Column(name="min_salary",columnDefinition = "int default 0")
	private int minSalary;
	
	@Column(name="max_salary",columnDefinition = "int default 0")
	private int maxSalary;

	@OneToMany(mappedBy = "job", cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REMOVE,
			CascadeType.REFRESH }, targetEntity = JobResponsibilitiesDto.class)
	@Builder.Default
	private List<JobResponsibilitiesDto> responsiblities = new ArrayList<>();

	@OneToMany(mappedBy = "job", cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REMOVE,
			CascadeType.REFRESH }, targetEntity = SkillsAndQualificationsDto.class)
	@Builder.Default
	private List<SkillsAndQualificationsDto> skillsAndQualifications = new ArrayList<>();

	@OneToMany(mappedBy = "job", cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REMOVE,
			CascadeType.REFRESH }, targetEntity = RoundDto.class)
	@Builder.Default
	private List<RoundDto> roundsAvailable = new ArrayList<>();

	public void addRound(RoundDto round) {
		roundsAvailable.add(round);
		round.setJob(this);
	}

	public void addSkill(SkillsAndQualificationsDto skill) {
		skillsAndQualifications.add(skill);
		skill.setJob(this);
	}

	public void addResponsibility(JobResponsibilitiesDto responsibility) {
		responsiblities.add(responsibility);
		responsibility.setJob(this);
	}
}
