package com.interview.tracker.dao.entity;

import java.util.Objects;

import com.interview.tracker.dao.dto.JobResponsibilitiesDto;
import com.interview.tracker.dao.dto.SkillsAndQualificationsDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SkillsAndQualificationsEntity {

	private long id;
	
	private String skill;
	
	private long jobId;
	
	public static SkillsAndQualificationsEntity formEntity(SkillsAndQualificationsDto dto) {
		if(Objects.isNull(dto)) return null;
		return SkillsAndQualificationsEntity.builder()
		  .skill(dto.getSkill())
		.build();
	}
	
	public static SkillsAndQualificationsDto formDto(SkillsAndQualificationsEntity entity) {
		if(Objects.isNull(entity)) return null;
		return SkillsAndQualificationsDto.builder()
		   .skill(entity.getSkill())
		.build();
	}
}
