package com.interview.tracker.dao.dao;

import java.util.Collection;
import java.util.Collections;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.interview.tracker.dao.dto.UserDto;

public class CustomUserDetailsDto implements  UserDetails{
	
	private static final long serialVersionUID = 1L;
	private UserDto userDto;
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return Collections.emptyList();
	}

	@Override
	public String getPassword() {
		
		return userDto.getPassword();
	}

	@Override
	public String getUsername() {
		
		return userDto.getUserName();
	}

	@Override
	public boolean isAccountNonExpired() {
		
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		
		return true;
	}

	@Override
	public boolean isEnabled() {
		
		return userDto.isEnabled();
	}
	
	public String email() {
		return userDto.getEmail();
	}

}
