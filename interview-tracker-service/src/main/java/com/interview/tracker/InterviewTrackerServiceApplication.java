package com.interview.tracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(exclude = {UserDetailsServiceAutoConfiguration.class})
@EnableJpaRepositories(basePackages = "com.interview.tracker.dao.repository")
@EntityScan(basePackages = {"com.interview.tracker.dao.dto"})
public class InterviewTrackerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(InterviewTrackerServiceApplication.class, args);
		
	}

}
