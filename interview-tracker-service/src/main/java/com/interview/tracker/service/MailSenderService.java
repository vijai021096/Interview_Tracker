package com.interview.tracker.service;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.mysql.cj.util.StringUtils;

import lombok.RequiredArgsConstructor;
@Service
public class MailSenderService {

	    @Autowired
	    private JavaMailSender javaMailSender;

	    @Value("${spring.mail.username: noreplyInterviewTracker@gmail.com}")
	    private String senderMail;
	    /**
	     * sendEmail is used to send mail to a recipient,
	     * @param mailSubject, the Subject to be displayed for the mail,
	     * @param mailContent, the actual body of the mail, each entry in list is a new line.
	     * @param recipient, the target mail to which the mail is to be sent,
	     * @param sender, the sender of the mail(Optional, if don't want to specify the sender pass as empty string)
	     * @param carbonCopyMails, the mail IDs for the carbon copying(Optional, pass empty or null if no CC needed)
	     * @param blindCarbonCopyMails, the mail IDs for the blind carbon copying,(Optional, pass empty or null if no BCC needed)
	     * @return void
	     * */
	    @Async
	    public void sendEmail(String mailSubject, List<String> mailContent,String recipient,String sender,
	    		List<String> carbonCopyMails,List<String> blindCarbonCopyMails)  throws IllegalArgumentException {
	    	if(CheckInvalidArguments(recipient)) {
	    		throw new IllegalArgumentException("Specify Correct Arguments");
	    	}
	    	 SimpleMailMessage mailMessage = new SimpleMailMessage();
	    	 if(StringUtils.isEmptyOrWhitespaceOnly(sender))
	    		 mailMessage.setFrom(this.senderMail);
	    	 else
	    		 mailMessage.setFrom(sender);
	         mailMessage.setSubject(mailSubject);
	         mailMessage.setTo(recipient);
	         if(!CollectionUtils.isEmpty(mailContent)) {
	        	 StringBuilder sb = new StringBuilder();
	        	 mailContent.stream().forEach(line ->{
	        		 sb.append(line);
	        		 sb.append(System.getProperty("line.separator"));
	        	 });
	        	mailMessage.setText(sb.toString());
	         }
	         if(Objects.nonNull(carbonCopyMails))
	        	 mailMessage.setCc(carbonCopyMails.toArray(new String[carbonCopyMails.size()]));
	         if(Objects.nonNull(blindCarbonCopyMails))
	        	 mailMessage.setBcc(blindCarbonCopyMails.toArray(new String[blindCarbonCopyMails.size()]));
	         javaMailSender.send(mailMessage);
	         
	    }
		private boolean CheckInvalidArguments(String recipient) {
			return StringUtils.isEmptyOrWhitespaceOnly(recipient);
		}
	
}
