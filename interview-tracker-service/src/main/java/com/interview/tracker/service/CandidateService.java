package com.interview.tracker.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.interview.tracker.controller.model.ApiResultModel;
import com.interview.tracker.controller.model.CandidateAppliedJobs;
import com.interview.tracker.controller.model.CandidateRejectionRequest;
import com.interview.tracker.controller.model.CandidateRequest;
import com.interview.tracker.controller.model.CandidateResponse;
import com.interview.tracker.controller.model.JobsAndCandidatesPerStatus;
import com.interview.tracker.controller.model.RoundModel;
import com.interview.tracker.dao.dao.CandidateDao;
import com.interview.tracker.dao.dao.JobDao;
import com.interview.tracker.dao.dao.UserDao;
import com.interview.tracker.dao.entity.CandidateEntity;
import com.interview.tracker.dao.entity.JobEntity;
import com.interview.tracker.dao.entity.RoundEntity;
import com.interview.tracker.dao.entity.UserEntity;

@Service
public class CandidateService {
	@Autowired
	private CandidateDao candidateDao;

	@Autowired
	private MailSenderService mailSender;

	@Autowired
	private JobService jobService;

	@Autowired
	private UserDao userDao;

	@Autowired
	private JobDao jobDao;

	/**
	 * used to fetch all the candidates for a job id
	 * 
	 * @param jobId
	 * @return
	 */
	public List<CandidateResponse> findCandidateByJobId(long jobId) {

		List<CandidateEntity> candidatesForJobId = candidateDao.findCandidatesByJobId(jobId);
		JobEntity jobs = jobDao.findJobById(jobId);
		if (CollectionUtils.isEmpty(candidatesForJobId)) {
			return Collections.emptyList();
		}
		return candidatesForJobId.stream().map(x -> {
			String match = jobService.getMatchingPercentage(jobs.getJobId(), x.getCandidateName());
			CandidateResponse ans = CandidateResponse.convertToResponse(x, jobs);
			ans.setNumberOfOpenings(jobs.getNumberOfOpenings());
			ans.setMatchingPercentage(match);
			return ans;
		}).collect(Collectors.toList());
	}

	/**
	 * 
	 * @param candidateId
	 * @param jobId
	 * @return
	 */
	public List<CandidateResponse> findCandidateByCandidateIdAndJobId(long candidateId, long jobId) {

		List<CandidateEntity> candidates = candidateDao.findCandidateByCandidateIdAndJobId(candidateId, jobId);
		JobEntity jobs = jobDao.findJobById(jobId);
		if (CollectionUtils.isEmpty(candidates)) {
			return Collections.emptyList();
		}
		return candidates.stream().map(x -> {
			CandidateResponse ans = CandidateResponse.convertToResponse(x, jobs);
			return ans;
		}).collect(Collectors.toList());
	}

	public List<CandidateResponse> findCandidateLikeName(long jobId, String candidateName, int offset) {
		List<CandidateEntity> candidates = candidateDao.findCandidatesLikeNameforAJob(jobId, candidateName, offset);
		JobEntity jobs = jobDao.findJobById(jobId);
		return candidates.stream().map(x -> {
			String match = jobService.getMatchingPercentage(jobs.getJobId(), x.getCandidateName());
			CandidateResponse ans = CandidateResponse.convertToResponse(x, jobs);
			ans.setNumberOfOpenings(jobs.getNumberOfOpenings());
			ans.setMatchingPercentage(match);
			return ans;
		}).collect(Collectors.toList());
	}

	@Transactional(rollbackOn = Exception.class)
	public ApiResultModel updateStatus(List<CandidateRequest> candidateList) {
		if (!CollectionUtils.isEmpty(candidateList)) {
			long jobId = candidateList.get(0).getJobId();
			JobEntity jobsResponse = jobDao.findJobById(jobId);
			List<String> rejectedCandidatesEmail = new ArrayList<>();
			List<CandidateEntity> insertCandidateList = candidateList.stream().map(candidate -> {
				if (candidate.getRoundNumber() == 0) {
					candidate.setStatus("IN PROGRESS");
				}
				if (jobsResponse.getNumberOfOpenings() > 0) {
					if (candidate.getRoundNumber() < jobsResponse.getNumOfRounds()) {
						candidate.setRoundNumber(candidate.getRoundNumber() + 1);
					} else {
						candidate.setRoundNumber(candidate.getRoundNumber() + 2);
						candidate.setStatus("SELECTED");
						jobsResponse.setNumberOfOpenings(jobsResponse.getNumberOfOpenings() - 1);
					}
				}
				CandidateEntity entity = candidate.convertToEntity();

				entity.setSheduledDate(candidate.getSheduledDate());
				entity.setTime(candidate.getTime());
				return entity;
			}).collect(Collectors.toList());

			boolean result = candidateDao.insertMultipleCandidates(insertCandidateList);
			if (jobsResponse.getNumberOfOpenings() == 0) {
				List<CandidateEntity> candidates = candidateDao.findCandidatesByJobId(jobId);
				candidates.stream().forEach(c -> {
					c.setStatus("REJECTED");
					rejectedCandidatesEmail.add(c.getEmailId());
					candidateDao.addCandidate(c);
				});
				jobsResponse.setStatus("CLOSED");
			}

			result = result && jobDao.addNewJob(jobsResponse);
			return this.sendMailForSelection(result, candidateList, jobsResponse, rejectedCandidatesEmail);
		}
		return new ApiResultModel(false, "failure");

	}

	public @SuppressWarnings("deprecation") ApiResultModel sendMailForSelection(boolean result,
			List<CandidateRequest> selectedCandidates, JobEntity jobsResponse, List<String> rejectedCandidatesEmail) {
		if (result && !CollectionUtils.isEmpty(selectedCandidates)) {
			if (!CollectionUtils.isEmpty(rejectedCandidatesEmail)) {
				List<String> mailContent = Arrays.asList("Sorry!!", "Dear Candidate" + ",",
						"Thank you so much for your interest in working with " + jobsResponse.getOrganisation()
								+ " and we appreciate you taking the time to interview with our team .\n We Regret to inform you that this position has been already filled");
				mailSender.sendEmail("Interview Status", mailContent, "noreplyInterviewTracker@gmail.com", "", null,
						rejectedCandidatesEmail);

			}
			if (selectedCandidates.size() == 1) {
				return this.sendMailForIndividualCandidate(selectedCandidates.get(0), jobsResponse,
						rejectedCandidatesEmail);
			} else {
				return this.sendMailForMultipleCandidates(selectedCandidates, jobsResponse, rejectedCandidatesEmail);
			}
		}
		return new ApiResultModel(false, "failure");
	}

	private ApiResultModel sendMailForMultipleCandidates(List<CandidateRequest> selectedCandidates,
			JobEntity jobsResponse, List<String> rejectedCandidatesEmail) {
		String scheduleMessage = selectedCandidates.get(0).getSheduledDate().getDate() + "/"
				+ (selectedCandidates.get(0).getSheduledDate().getMonth() + 1) + "/"
				+ (selectedCandidates.get(0).getSheduledDate().getYear() + 1900) + " " + "." + " Time:" + " "
				+ selectedCandidates.get(0).getTime() + ".";
		List<String> shorListedCandidateMails = selectedCandidates.stream()
				.filter(candidate -> candidate.getRoundNumber() == 1).map(candidate -> candidate.getEmailId())
				.collect(Collectors.toList());
		List<String> processedCandidates = selectedCandidates.stream()
				.filter(candidate -> candidate.getRoundNumber() <= jobsResponse.getNumOfRounds()
						&& candidate.getRoundNumber() != 1)
				.map(candidate -> candidate.getEmailId()).collect(Collectors.toList());
		List<String> finalCandidates = selectedCandidates.stream()
				.filter(candidate -> !shorListedCandidateMails.contains(candidate.getEmailId())
						&& !processedCandidates.contains(candidate.getEmailId()))
				.map(candidate -> candidate.getEmailId()).collect(Collectors.toList());
		List<String> selectedMailContent = Arrays.asList("Congratulations!!", "Dear Canidate,",
				"You are shortlisted for this job.  First round is scheduled on " + scheduleMessage);
		mailSender.sendEmail("Interview Status", selectedMailContent, "noreplyInterviewTracker@gmail.com", "", null,
				shorListedCandidateMails);
		List<String> processMailContent = Arrays.asList("Congratulations!!", "Dear Canidate,",
				"You have succesfully cleared the current round." + "The Next round is scheduled on "
						+ scheduleMessage);
		mailSender.sendEmail("Interview Status", processMailContent, "noreplyInterviewTracker@gmail.com", "", null,
				processedCandidates);
		List<String> finalMailContent = Arrays.asList("Congratulations!!", "Dear Candidate,",
				"You have succesfully cleared the Interview Process!!");
		mailSender.sendEmail("Interview Status", finalMailContent, "noreplyInterviewTracker@gmail.com", "", null,
				finalCandidates);
		return new ApiResultModel(true, "success");

	}

	private ApiResultModel sendMailForIndividualCandidate(CandidateRequest candidate, JobEntity jobsResponse,
			List<String> rejectedCandidatesEmail) {
		if (candidate.getRoundNumber() == 1) {
			List<String> mailContent = Arrays.asList("Congratulations!!", "Dear " + candidate.getCandidateName() + ",",
					"You are shortlisted for this job.  First round is scheduled on "
							+ candidate.getSheduledDate().getDate() + "/" + (candidate.getSheduledDate().getMonth() + 1)
							+ "/" + (candidate.getSheduledDate().getYear() + 1900) + " " + "." + " Time:" + " "
							+ candidate.getTime() + ".");
			mailSender.sendEmail("Interview Status", mailContent, candidate.getEmailId(), "", null, null);
		} else if (candidate.getRoundNumber() <= jobsResponse.getNumOfRounds()) {

			List<String> mailContent = Arrays.asList("Congratulations!!", "Dear " + candidate.getCandidateName() + ",",
					"You have succesfully cleared the round:" + " " + (candidate.getRoundNumber() - 1) + ". "
							+ "The Next round is scheduled on " + candidate.getSheduledDate().getDate() + "/"
							+ (candidate.getSheduledDate().getMonth() + 1) + "/"
							+ (candidate.getSheduledDate().getYear() + 1900) + " " + "." + " Time:" + " "
							+ candidate.getTime() + ".");
			mailSender.sendEmail("Interview Status", mailContent, candidate.getEmailId(), "", null, null);
		} else {
			List<String> mailContent = Arrays.asList("Congratulations!!", "Dear " + candidate.getCandidateName() + ",",
					"You have succesfully cleared the Interview Process!!");
			mailSender.sendEmail("Interview Status", mailContent, candidate.getEmailId(), "", null, null);
		}

		return new ApiResultModel(true, "success");
	}

	public ApiResultModel rejectionStatus(List<CandidateRejectionRequest> candidateList) {

		if (!CollectionUtils.isEmpty(candidateList)) {
			JobEntity job = jobDao.findJobById(candidateList.get(0).getJobId());
			List<CandidateEntity> rejectedCandidates = candidateList.stream().map(request -> {
				CandidateEntity entity = request.convertToEntity();
				entity.setStatus("REJECTED");
				return entity;
			}).collect(Collectors.toList());

			boolean result = candidateDao.addMultipleCandidates(rejectedCandidates);
			if (result) {
				List<String> mailContent = Arrays.asList(
						"Dear " + (rejectedCandidates.size() == 1 ? rejectedCandidates.get(0).getCandidateName()
								: "Candidate") + ",",
						"Thank you so much for your interest in working with " + job.getOrganisation()
								+ " and we appreciate you taking the time to interview with our team. After careful evaluation we regret to inform you that you are not selected. Feedback from the team as below. \n"
								+ rejectedCandidates.get(0).getFeedBack());
				mailSender.sendEmail("Interview Status", mailContent, "noreplyInterviewTracker@gmail.com", "", null,
						rejectedCandidates.stream().map(CandidateEntity::getEmailId).collect(Collectors.toList()));

				return new ApiResultModel(true, "success");
			}
		}
		return new ApiResultModel(false, "failure");

	}

	public List<CandidateEntity> findByCandidateName(String candidateName) {
		return candidateDao.findCandidatesByCandidateName(candidateName);
	}

	public ApiResultModel applyJob(String candidateName, long jobId) {
		UserEntity user = userDao.findUserByName(candidateName);
		if (Objects.nonNull(user)) {
			CandidateEntity candidate = CandidateEntity.builder().candidateId(user.getUserId()).jobId(jobId)
					.candidateName(candidateName).emailId(user.getEmail()).roundNumber(0).status("APPLIED").build();
			boolean result = candidateDao.addCandidate(candidate);
			if (result) {
				return new ApiResultModel(true, "Applied successfully");
			}
		}
		return new ApiResultModel(false, "Application failed");
	}

	public List<CandidateAppliedJobs> getAppliedJobsByCandidateName(String candidateName) {
		List<CandidateEntity> candidatesForCandidateName = candidateDao.findCandidatesByCandidateName(candidateName);
		if (Objects.nonNull(candidatesForCandidateName)) {
			List<Long> candidateJobList = candidatesForCandidateName.stream().map(CandidateEntity::getJobId)
					.collect(Collectors.toList());
			if (Objects.nonNull(candidateJobList)) {
				List<JobEntity> jobList = jobDao.findJobsFromJobIdArray(candidateJobList);
				List<CandidateEntity> candidateList = candidateDao.getCandidatesByMultipleJobIds(candidateJobList);
				Map<Long, List<CandidateEntity>> candidateGroupedMap = candidateList.stream()
						.filter(candidateDetail -> !candidateDetail.getCandidateName().equals(candidateName))
						.collect(Collectors.groupingBy(candidate -> candidate.getJobId()));

				Map<Long, JobEntity> jobMapList = jobList.stream()
						.collect(Collectors.toMap(job -> job.getJobId(), job -> job));

				return candidatesForCandidateName.stream().map(candidateJob -> {
					JobEntity jobInfo = jobMapList.get(candidateJob.getJobId());
					int noOfCompetitors = candidateGroupedMap.getOrDefault(candidateJob.getJobId(), new ArrayList<>())
							.stream()
							.filter(otherCandidate -> Objects.nonNull(otherCandidate)
									&& otherCandidate.getRoundNumber() >= candidateJob.getRoundNumber())
							.collect(Collectors.toList()).size();
					jobInfo.getAvailableOpenings().sort(new Comparator<RoundEntity>() {
						@Override
						public int compare(RoundEntity roundInfo1, RoundEntity roundInfo2) {
							return roundInfo1.getRoundNo() - roundInfo2.getRoundNo();
						}
					});
					return CandidateAppliedJobs.builder().candidateName(candidateJob.getCandidateName())
							.jobName(jobInfo.getJobName()).jobShortDescription(jobInfo.getJobDescription())
							.competingWith(noOfCompetitors).roundNumber(candidateJob.getRoundNumber())
							.Status(candidateJob.getStatus()).organisation(jobInfo.getOrganisation())
							.rounds(jobInfo.getAvailableOpenings().stream().map(round -> {
								return RoundModel.builder().roundName(round.getRoundName())
										.roundDescription(round.getRoundDescription()).roundNo(round.getRoundNo())
										.build();
							}).collect(Collectors.toList())).feedBack(candidateJob.getFeedBack())
							.scheduledDate(
									Objects.nonNull(candidateJob.getSheduledDate())
											? candidateJob.getSheduledDate().toString().substring(0, 10) + " "
													+ candidateJob.getTime()
											: "")
							.build();
				}).collect(Collectors.toList());
			}
		}
		return null;

	}

	public List<JobsAndCandidatesPerStatus> getAllCandidatesPerJobStatus(String status, String userName) {
		UserEntity user = userDao.findUserByName(userName);
		List<JobEntity> jobs = jobDao.getAllJobsPerStatus(status, user.getUserId());
		Map<Long, JobEntity> jobInfoMap = jobs.stream().collect(Collectors.toMap(job -> job.getJobId(), job -> job));
		List<Long> jobIds = jobs.stream().map(JobEntity::getJobId).collect(Collectors.toList());
		Map<Long, List<CandidateEntity>> groupedMap = candidateDao.findAllCandidatesByMultipleJobIds(jobIds).stream()
				.collect(Collectors.groupingBy(candidate -> candidate.getJobId()));
		return jobInfoMap.entrySet().stream().filter(entry -> groupedMap.containsKey(entry.getKey())).map(entry -> {
			long jobId = entry.getKey();
			return JobsAndCandidatesPerStatus.builder().jobName(jobInfoMap.get(jobId).getJobName()).jobId(jobId)
					.numberOfCandidates(groupedMap.get(jobId).size()).build();
		}).collect(Collectors.toList());
	}
}