package com.interview.tracker.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.interview.tracker.controller.model.ApiResultModel;
import com.interview.tracker.controller.model.AssignRolesUserRequestModel;
import com.interview.tracker.controller.model.IndividualExperience;
import com.interview.tracker.controller.model.OTPRequestModel;
import com.interview.tracker.controller.model.RolesResponseModel;
import com.interview.tracker.controller.model.UserProfileModel;
import com.interview.tracker.controller.model.UserResponseModel;
import com.interview.tracker.dao.dao.ExperienceDao;
import com.interview.tracker.dao.dao.RoleDao;
import com.interview.tracker.dao.dao.UserDao;
import com.interview.tracker.dao.dto.UserDto;
import com.interview.tracker.dao.entity.ExperienceEntity;
import com.interview.tracker.dao.entity.RoleEntity;
import com.interview.tracker.dao.entity.UserEntity;

@Service
public class UserService {

	@Autowired
	private UserDao userDao;

	@Autowired
	@Qualifier("passwordEncoder")
	private BCryptPasswordEncoder bcryptPasswordEncoder;

	@Autowired
	private MailSenderService mailSender;

	@Autowired
	private RoleDao roleDao;

	@Autowired
	private ExperienceDao experienceDao;

	/**
	 * 
	 * @param userId
	 * @return user details
	 */
	public UserDto findByUSerId(long userId) {

		return UserEntity.FormDto(userDao.findByUserId(userId));

	}

	/**
	 * 
	 * @param user
	 * @return
	 */
	@Transactional(rollbackOn = Exception.class)
	public ApiResultModel registerUser(UserEntity user) {
		String pass = bcryptPasswordEncoder.encode(user.getPassword());
		user.setPassword(pass);
		user.setRoles(Arrays.asList(RoleEntity.builder().roleName("CANDIDATE").build()));
		try {
			int randomOTP = this.generateOTP();
			user.setVerificationOTP(randomOTP);
			userDao.registerUser(user);
			List<String> mailContent = Arrays.asList("Welcome!!", "Dear " + user.getUserName() + ",",
					"The OTP for your Email Verification is " + randomOTP + ".");
			mailSender.sendEmail("Complete Registration", mailContent, user.getEmail(), "", null, null);
			return new ApiResultModel(true, "Registration Success");
		} catch (DataIntegrityViolationException ex) {
			return new ApiResultModel(false, "User AlreadyRegistered");
		}
	}

	public UserDetails findByUserName(String userName) {

		return userDao.findByUserName(userName);
	}

	public UserResponseModel findUserByName(String userName) {
		UserEntity userEntity = userDao.findUserByName(userName);
		if (Objects.nonNull(userEntity)) {
			return UserResponseModel.builder().userName(userEntity.getUserName())
					.roles(userEntity.getRoles().stream().map(RoleEntity::getRoleName).collect(Collectors.toList()))
					.build();
		}
		return null;
	}

	private int generateOTP() {
		return ThreadLocalRandom.current().nextInt(1000, 9999);
	}

	public boolean verifyOtp(OTPRequestModel request) {
		UserEntity userEntity = userDao.findUserByName(request.getUserName());
		if (Objects.nonNull(userEntity) && request.getOtpEntered() == userEntity.getVerificationOTP()) {
			userEntity.setEnabled(true);
			userEntity.setVerificationOTP(0);
			return userDao.updateOrSave(userEntity);
		}
		return false;
	}

	@Transactional(rollbackOn = Exception.class)
	public boolean resendOtp(OTPRequestModel request) {
		UserEntity userEntity = userDao.findUserByName(request.getUserName());
		if (Objects.nonNull(userEntity)) {
			int oneTimePassword = this.generateOTP();
			userEntity.setVerificationOTP(oneTimePassword);
			if (userDao.updateOrSave(userEntity)) {
				String mailContent = "The OTP for your Email Verification is " + oneTimePassword + ".";
				mailSender.sendEmail("Complete Registration", Arrays.asList(mailContent), userEntity.getEmail(), "",
						null, null);
				return true;
			}

		}
		return false;
	}

	public UserResponseModel findUserByEmailId(String emailId) {
		UserEntity userEntity = userDao.findUserByEmailId(emailId);
		if (Objects.nonNull(userEntity)) {
			return UserResponseModel.builder().userName(userEntity.getUserName())
					.roles(userEntity.getRoles().stream().map(RoleEntity::getRoleName).collect(Collectors.toList()))
					.build();
		}
		return null;
	}

	public List<UserResponseModel> findAllUsersExcept(AssignRolesUserRequestModel request) {
		return userDao.getAllUsersExcept(request.getUserName(), request.getKeyword(), request.getOffset()).stream()
				.map(user -> {
					if (Objects.nonNull(user)) {
						return UserResponseModel.builder().userName(user.getUserName()).roles(
								user.getRoles().stream().map(RoleEntity::getRoleName).collect(Collectors.toList()))
								.build();
					}
					return null;
				}).collect(Collectors.toList());
	}

	public RolesResponseModel getUserRoles(String userName) {
		List<String> availableRoles = Arrays.asList("ADMIN", "HR", "CANDIDATE");
		UserEntity userEntity = userDao.findUserByName(userName);
		if (Objects.nonNull(userEntity)) {
			List<String> userRoles = userEntity.getRoles().stream().map(RoleEntity::getRoleName)
					.collect(Collectors.toList());
			return RolesResponseModel.builder().roleName(userRoles.stream().findFirst().orElse(""))
					.alreadyGivenRole(true).build();

		} else {
			throw new IllegalStateException("Roles cannot be fetched for unknown User: " + userName);
		}
	}

	@Transactional(rollbackOn = Exception.class)
	public ApiResultModel updateRoles(UserResponseModel request) {
		UserEntity userEntity = userDao.findUserByName(request.getUserName());
		if (Objects.nonNull(userEntity)) {
			boolean result = roleDao.deleteRolesByUserId(userEntity.getUserId());
			if (result) {
				userEntity.setRoles(request.getRoles().stream().map(role -> RoleEntity.builder().roleName(role).build())
						.collect(Collectors.toList()));
				boolean userResult = userDao.updateOrSave(userEntity);
				return userResult ? new ApiResultModel(true, "Inserstion Success")
						: new ApiResultModel(false, "Insertion Failure");
			}
		} else {
			return new ApiResultModel(false, "Role Update Failure");
		}
		return new ApiResultModel(false, "Role Update Failure");

	}

	@Transactional(rollbackOn = Exception.class)
	public ApiResultModel saveProfile(UserProfileModel userProfileModel) {
		UserEntity userEntity = userDao.findUserByName(userProfileModel.getUserName());
		if (Objects.nonNull(userEntity)) {
			experienceDao.deleteAllExperienceOfUser(userEntity.getUserId());
			userEntity.setGender(userProfileModel.getGender());
			userEntity.setHighestQualification(userProfileModel.getHighestQualification());
			userEntity.setSkills(userProfileModel.getSkills().stream().collect(Collectors.joining(",")));
			userEntity.setTotalExperience(userProfileModel.getTotalExp());
			userEntity.setExperience(userProfileModel.getCompanyExpList().stream()
					.map(IndividualExperience::convertToEntity).collect(Collectors.toList()));
			userDao.updateOrSave(userEntity);
			return new ApiResultModel(true, "Profile Updated");
		}
		return new ApiResultModel(false, "Profile not updated");
	}

	public UserProfileModel getUserProfile(String userName) {
		UserEntity userEntity = userDao.findUserByName(userName);
		if (Objects.nonNull(userEntity)) {
			List<ExperienceEntity> experience = experienceDao.getUserExperience(userEntity.getUserId());
			return UserProfileModel.builder().userName(userEntity.getUserName())
					.companyExpList(
							experience.stream().map(IndividualExperience::convertToModel).collect(Collectors.toList()))
					.email(userEntity.getEmail()).gender(userEntity.getGender())
					.highestQualification(userEntity.getHighestQualification())
					.roles(userEntity.getRoles().stream().map(RoleEntity::getRoleName).collect(Collectors.joining(",")))
					.skills(Objects.isNull(userEntity.getSkills()) || userEntity.getSkills().equals("")
							? new ArrayList<String>()
							: Arrays.asList(userEntity.getSkills().split(",")))
					.totalExp(userEntity.getTotalExperience()).build();
		}
		return UserProfileModel.builder().build();
	}
}
