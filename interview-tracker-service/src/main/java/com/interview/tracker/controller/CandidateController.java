package com.interview.tracker.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.interview.tracker.controller.model.ApiResultModel;
import com.interview.tracker.controller.model.CandidateAppliedJobs;
import com.interview.tracker.controller.model.CandidateRejectionRequest;
import com.interview.tracker.controller.model.CandidateRequest;
import com.interview.tracker.controller.model.CandidateResponse;
import com.interview.tracker.controller.model.JobsAndCandidatesPerStatus;
import com.interview.tracker.dao.dao.CandidateDao;
import com.interview.tracker.dao.dao.JobDao;
import com.interview.tracker.service.CandidateService;
import com.interview.tracker.service.JobService;

@RestController
@RequestMapping(value = "/api/candidate")
public class CandidateController {
	@Autowired
	private CandidateService candidateService;

	@Autowired
	private JobDao jobDao;

	@Autowired
	private JobService jobService;

	@Autowired
	private CandidateDao candidateDao;

	@PostMapping(value = "/applyJob")
	public ApiResultModel applyJob(@RequestParam("candiateName") String candidateName,
			@RequestParam("jobId") long jobId) {
		return candidateService.applyJob(candidateName, jobId);
	}

	@GetMapping(value = "/getCandidatesByJobId")
	public List<CandidateResponse> findCandidatesforAJob(@RequestParam("jobId") long jobId) {
		List<CandidateResponse> candidatesByJobId = candidateService.findCandidateByJobId(jobId);
		return candidatesByJobId;
	}

	@GetMapping(value = "/getAppliedJobs/{candidateName}")
	public List<CandidateAppliedJobs> findCandidatesforAJob(@PathVariable("candidateName") String candidateName) {
		List<CandidateAppliedJobs> candidatesWithJob = candidateService.getAppliedJobsByCandidateName(candidateName);
		return candidatesWithJob;
	}

	@PostMapping(value = "/selectCandidate")
	public ApiResultModel selectCandidateForNextRound(@RequestBody List<CandidateRequest> request) {
		return candidateService.updateStatus(request);
	}

	@PostMapping(value = "/rejectCandidate")
	public ApiResultModel rejectCandidateForNextRound(@RequestBody List<CandidateRejectionRequest> request) {
		return candidateService.rejectionStatus(request);
	}

	@GetMapping(value = "/hr/getAllCandidatesPerJobStatus")
	public List<JobsAndCandidatesPerStatus> getAllCandidatesPerJobStatus(@RequestParam("status") String status,
			@RequestParam("userName") String userName) {
		return candidateService.getAllCandidatesPerJobStatus(status, userName);
	}

	@GetMapping(value="/getAllCandidatesLikeName")
	public List<CandidateResponse>findCandidatesLikeName(@RequestParam("jobId")long jobId,@RequestParam("candidateName") String candidateName,@RequestParam("offset")int offset){
		return candidateService.findCandidateLikeName(jobId, candidateName, offset);
	}
}
