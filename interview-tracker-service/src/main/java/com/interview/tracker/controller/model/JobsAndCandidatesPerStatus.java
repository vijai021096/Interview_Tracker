package com.interview.tracker.controller.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JobsAndCandidatesPerStatus {

	private String jobName;
	private int numberOfCandidates;
	private long jobId;
}
