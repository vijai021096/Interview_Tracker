package com.interview.tracker.controller.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class CandidateAppliedJobs {
	private String candidateName;
	private String jobName;
	private String jobShortDescription;
	private String Status;
	private String feedBack;
	private String organisation;
	private String scheduledDate;
	private List<RoundModel> rounds;
	private int competingWith;

	private Number roundNumber;
}
