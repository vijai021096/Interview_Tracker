package com.interview.tracker.controller.model;

import com.interview.tracker.dao.entity.ExperienceEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class IndividualExperience {

	private String companyName;

	private int companyExp;

	public static ExperienceEntity convertToEntity(IndividualExperience request) {
		return ExperienceEntity.builder().companyName(request.getCompanyName()).experience(request.getCompanyExp())
				.build();
	}

	public static IndividualExperience convertToModel(ExperienceEntity entity) {
		return IndividualExperience.builder().companyName(entity.getCompanyName()).companyExp(entity.getExperience())
				.build();
	}
}
