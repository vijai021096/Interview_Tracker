package com.interview.tracker.controller.model;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

import com.interview.tracker.dao.entity.JobEntity;
import com.interview.tracker.dao.entity.JobResponsibilitiesEntity;
import com.interview.tracker.dao.entity.SkillsAndQualificationsEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class JobsResponse {
	private long jobId;
	private String jobName;
	private String jobShortDescription;
	private int minExperience;
	private int maxExperience;
	private int numberOfOpenings;
	private int numOfRounds;
	private String jobStatus;
	private String organisation;
	private String appliedStatus;
	private String candidateStatus;
	private int minSalary;
	private int maxSalary;
	private String salaryRange;
	private long pic;
	private List<String> responsibilities;
	private List<String> skillsAndQualification;
	private List<RoundModel> rounds;
	private Date scheduledDate;
	private String time;

	/**
	 * Used to convert the jobEntity from DAO layer to the view response
	 */
	public static JobsResponse convertToResponse(JobEntity jobEntity) {
		if (Objects.nonNull(jobEntity)) {
			JobsResponse response = new JobsResponse();
			response.setJobId(jobEntity.getJobId());
			response.setOrganisation(jobEntity.getOrganisation());
			response.setJobName(jobEntity.getJobName());
			response.setJobShortDescription(jobEntity.getJobDescription());
			response.setMinExperience(jobEntity.getMinExperience());
			response.setMaxExperience(jobEntity.getMaxExperience());
			response.setNumberOfOpenings(jobEntity.getNumberOfOpenings());
			response.setNumOfRounds(jobEntity.getNumOfRounds());
			response.setMaxSalary(jobEntity.getMaxSalary());
			response.setMinSalary(jobEntity.getMinSalary());
			response.setJobStatus(jobEntity.getStatus());
			response.setPic(jobEntity.getPersonIncharge());
			if (!CollectionUtils.isEmpty(jobEntity.getResponsibilities())) {
				response.setResponsibilities(jobEntity.getResponsibilities().stream()
						.map(JobResponsibilitiesEntity::getResponsiblities).collect(Collectors.toList()));
			}

			if (!CollectionUtils.isEmpty(jobEntity.getSkillsAndQualifications())) {
				response.setSkillsAndQualification(jobEntity.getSkillsAndQualifications().stream()
						.map(SkillsAndQualificationsEntity::getSkill).collect(Collectors.toList()));
			}

			if (!CollectionUtils.isEmpty(jobEntity.getAvailableOpenings())) {
				response.setRounds(jobEntity
						.getAvailableOpenings().stream().map(round -> RoundModel.builder()
								.roundName(round.getRoundName()).roundDescription(round.getRoundDescription()).build())
						.collect(Collectors.toList()));
			}
			return response;
		}
		return null;
	}

	/*
	 * public static RegisterNewJobRequest convertToRequest(JobsResponse
	 * jobResponse) { if(Objects.nonNull(jobResponse)) { RegisterNewJobRequest
	 * request = new RegisterNewJobRequest();
	 * request.setJobName(jobResponse.getJobName());
	 * request.setJobShortDescription(jobResponse.getJobShortDescription());
	 * request.setMaxExperience(jobResponse.getMaxExperience());
	 * request.setMinExperience(jobResponse.getMinExperience());
	 * request.setNumberOfOpenings(jobResponse.getNumberOfOpenings());
	 * request.setPersonInCharge(jobResponse.getPic());
	 * request.setStatus(jobResponse.getJobStatus()); return request; } return null;
	 * 
	 * 
	 * }
	 */
}
