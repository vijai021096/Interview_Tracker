package com.interview.tracker.controller.model;

import java.util.Date;
import java.util.Objects;

import com.interview.tracker.dao.entity.CandidateEntity;
import com.interview.tracker.dao.entity.JobEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class CandidateResponse {

	private long jobId;

	private long candidateId;

	private String candidateName;

	private int roundNumber;

	private String status;

	private String emailId;

	private Date sheduledDate;

	private String time;

	private int experience;

	private String skills;

	private int totalRounds;

	private String matchingPercentage;

	private int numberOfOpenings;

	/**
	 * Used to convert the jobEntity from DAO layer to the view response
	 */
	public static CandidateResponse convertToResponse(CandidateEntity candidateEntity, JobEntity jobEntity) {
		if (Objects.nonNull(candidateEntity)) {
			CandidateResponse response = new CandidateResponse();
			response.setJobId(candidateEntity.getJobId());
			response.setCandidateId(candidateEntity.getCandidateId());
			response.setCandidateName(candidateEntity.getCandidateName());
			response.setRoundNumber(candidateEntity.getRoundNumber());
			response.setStatus(candidateEntity.getStatus());
			response.setEmailId(candidateEntity.getEmailId());
			response.setSheduledDate(candidateEntity.getSheduledDate());
			response.setTime(candidateEntity.getTime());
			response.setTotalRounds(jobEntity.getNumOfRounds());
			return response;
		}
		return null;
	}
}
