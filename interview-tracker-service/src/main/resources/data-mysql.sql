INSERT INTO `user_info`
(`user_id`,
`email_id`,
`is_enabled`,
`password`,
`user_name`,
`verification_otp`)
VALUES
(0,
'admin@gmail.com',
1,
'$2a$10$S5c1zDE6o7qNgtWa4XbSCeWvUS5CDI6RXEd/jXIcMKOIjVbyulMbu',
'admin',
0) ON duplicate key update verification_otp=0;
COMMIT;
SELECT user_id INTO @user_id FROM user_info WHERE user_name='admin' LIMIT 1;
INSERT INTO `user_role`
(`role_id`,
`role_name`,
`user_id`)
VALUES(
0,
'ADMIN',(
SELECT @user_id)) ON duplicate key update role_name='ADMIN';
COMMIT;

